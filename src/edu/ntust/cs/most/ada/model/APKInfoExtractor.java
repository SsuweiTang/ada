package edu.ntust.cs.most.ada.model;

import edu.ntust.cs.most.ada.domain.APKManifest;


/**
 * 
 * @author Ssu-Wei,Tang
 *
 */
public interface APKInfoExtractor {
	
	public  APKManifest extractManifestInfo(String apkFilePath,String apkFileName,int versionCode);

}
