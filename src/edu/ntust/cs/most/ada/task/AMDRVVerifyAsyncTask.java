package edu.ntust.cs.most.ada.task;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import edu.ntust.cs.most.ada.activity.ApkFileSub5Activity;
import edu.ntust.cs.most.ada.activity.DetailResultSub5Activity;
import edu.ntust.cs.most.ada.activity.ResultActivity;
import edu.ntust.cs.most.ada.domain.APKManifest;

/**
 * 
 * @author Ssu-Wei,Tang
 *
 */
@SuppressWarnings("deprecation")
public class AMDRVVerifyAsyncTask extends AsyncTask<Void, Void, Void> {
	private APKManifest apkManifest;
	private MultiValueMap<String, Object> map;
	private HttpHeaders headers;
	private String resultCode;
	private String resultDetail;
	private Context mContext;
	
	private ProgressDialog progress = null;
	String PathFile;
	String UploadFilesResponse;
	SimpleDateFormat sDateFormat = new SimpleDateFormat("yyy/MM ");
	String apkuploadDate = sDateFormat.format(new java.util.Date());

	public AMDRVVerifyAsyncTask(Context context) {

		mContext = context;
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		/*faker*/
		Log.i("Main", " loading URL: " + ApkFileSub5Activity.apkFilePath);
		List<NameValuePair> params1 = new ArrayList<NameValuePair>();
		params1.add(new BasicNameValuePair("file", ApkFileSub5Activity.apkFilePath));
		params1.add(new BasicNameValuePair("projectName", ApkFileSub5Activity.apkFileName+ApkFileSub5Activity.versionCode));
//		params1.add(new BasicNameValuePair("apkname", apk_packagename));
//		params1.add(new BasicNameValuePair("apkversion", apk_version));
//		params1.add(new BasicNameValuePair("apkuploaddate", apkuploadDate));
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(
				"http://140.118.110.84:8080/ue-platform/project/uploadForManualTestingForMobile");
		try {
			// setup multipart entity
			@SuppressWarnings("deprecation")
			MultipartEntity entity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			for (int i = 0; i < params1.size(); i++) {
				// identify param type by Key
				if (params1.get(i).getName().equals("file")) {
					File f = new File(params1.get(i).getValue());
					FileBody fileBody = new FileBody(f);
					entity.addPart("apkFile", fileBody);

				} else {
					entity.addPart(params1.get(i).getName(),
							new StringBody(params1.get(i).getValue()));
				}
			}
			post.setEntity(entity);

			// create response handler
			ResponseHandler<String> handler = new BasicResponseHandler();
			// execute and get response
			UploadFilesResponse = new String(client.execute(post, handler)
					.getBytes(), HTTP.UTF_8);
			
		} catch (Exception e) {
			e.printStackTrace();
		}//finally{
			//Log.i("Main", "UploadFilesResponse: " + UploadFilesResponse);
			return null;
		//}		
	}

	@Override
	protected void onPreExecute() {
		
		super.onPreExecute();
		ApkFileSub5Activity.showProgressDialog();
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

		jumpToResult();
		ApkFileSub5Activity.progressDialog.dismiss();

	}

	@Override
	protected void onCancelled() {
		super.onCancelled();

	}


	protected void jumpToResult() {
		Intent intent = new Intent();
		intent.setClass(mContext, DetailResultSub5Activity.class);
		Bundle bundle = new Bundle();
		bundle.putString("resultCode", resultCode);
		bundle.putString("resultDetail", resultDetail);
		bundle.putString("apkFilePath", ApkFileSub5Activity.apkFilePath);
		bundle.putString("apkFileName", ApkFileSub5Activity.apkFileName);
		bundle.putString("versionCode", String.valueOf(ApkFileSub5Activity.versionCode));
		bundle.putString("UploadFilesResponse", UploadFilesResponse);
		intent.putExtras(bundle);
		mContext.startActivity(intent);
		 ((Activity)mContext).finish();
	}
}