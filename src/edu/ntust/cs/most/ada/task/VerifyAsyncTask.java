package edu.ntust.cs.most.ada.task;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import edu.ntust.cs.most.ada.R;
import edu.ntust.cs.most.ada.activity.ApkFileActivity;
import edu.ntust.cs.most.ada.activity.ResultActivity;
import edu.ntust.cs.most.ada.domain.APKManifest;
import edu.ntust.cs.most.ada.model.APKInfoExtractor;
import edu.ntust.cs.most.ada.modelImpl.APKInfoExtractorImpl;
import edu.ntust.cs.most.ada.parameter.ServiceURL;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;

/**
 * 
 * @author Ssu-Wei,Tang
 *
 */
public class VerifyAsyncTask extends AsyncTask<Void, Integer, String> {
	private APKManifest apkManifest;
	private MultiValueMap<String, Object> map;
	private HttpHeaders headers;
	private String resultCode;
	private String resultDetail;
	private Context mContext;

	public VerifyAsyncTask(Context context) {

		mContext = context;		
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();		
		
		ApkFileActivity.showProgressDialog();		
		APKInfoExtractorImpl apkInfoExtractorService = new APKInfoExtractorImpl();
		apkManifest = apkInfoExtractorService.extractManifestInfo(
				ApkFileActivity.apkFilePath, ApkFileActivity.apkFileName,
				ApkFileActivity.versionCode);
		map = new LinkedMultiValueMap<String, Object>();
		map.add("file", new FileSystemResource(new File(
				ApkFileActivity.apkFilePath)));
		map.add("filename", ApkFileActivity.apkFileName);
		map.add("androidManifestXml", apkManifest.getShaForAndroidManifestXml());
		map.add("resourcesArsc", apkManifest.getShaForResourcesArsc());
		map.add("classesDex", apkManifest.getShaForClassesDex());
		map.add("mnftEntrySize",
				Integer.toString(apkManifest.getMnftEntrySize()));
		map.add("versionCode", apkManifest.getVersionCode());
		headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);	
		
		
		System.out.println(ApkFileActivity.apkFileName);
		System.out.println(apkManifest.getShaForAndroidManifestXml());
		System.out.println(apkManifest.getShaForResourcesArsc());
		System.out.println(apkManifest.getShaForClassesDex());
		System.out.println(apkManifest.getMnftEntrySize());
		System.out.println(apkManifest.getVersionCode());
		
	}

	@Override
	protected String doInBackground(Void... arg0) {
		ResponseEntity<String> response = null;
		try {
			RestTemplate restTemplate = new RestTemplate(true);
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
			requestFactory.setConnectTimeout(4000);
			restTemplate.setRequestFactory(requestFactory);
			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(
					map, headers);
			try {
				response = restTemplate.exchange(
						ServiceURL.SHA1ANDFINGERPRINT_VERIFICATION,
						HttpMethod.POST, requestEntity, String.class);
				if (response.getStatusCode().equals(HttpStatus.OK))
					return response.getBody();
			} catch (HttpClientErrorException e) {
				Log.e("Network Error ", e.getLocalizedMessage(), e);
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		String warm = "";
		try {
			JSONObject jsonResponse = null;

			if (result == null) {
				resultCode = "0";
			} else {
				jsonResponse = new JSONObject(result);
				if (jsonResponse.getString("upload").equals("success")) {
					if (jsonResponse.getInt("verifyResult") == 1) {
						resultCode = "1";
					} else if (jsonResponse.getInt("verifyResult") == 3) {
						resultCode = "3";
					} else if (jsonResponse.getInt("verifyResult") == 2) {
						resultCode = "2";
					} else if (jsonResponse.getInt("verifyResult") == 4) {
						resultCode = "4";
						String[] AfterSplitError = jsonResponse.getString(
								"verifyError").split(",");
						for (int i = 0; i < AfterSplitError.length; i++) {
							warm +=" * "+ AfterSplitError[i] + "\n";
						}
						warm = warm.substring(0, warm.length() - 1);
						resultDetail = warm;

					}
				} else {
					resultCode = "5";

				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		handler.post(myRunnable);
	
		
		
		
//		jumpToResult();
		
		
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();

	}
	
	

	
	private Handler handler = new Handler();

	private Runnable myRunnable = new Runnable() {
		public void run() {

			if (ApkFileActivity.pravTask != null
					&& ApkFileActivity.pravTask.getStatus() == AsyncTask.Status.FINISHED) {
				ApkFileActivity.progressDialog.dismiss();
				jumpToResult();

			} else {
				
				handler.postDelayed(this, 2000);
			}

		}
	};
	
	
	
	protected void jumpToResult() {
		Intent intent = new Intent();
		intent.setClass(mContext, ResultActivity.class);
		Bundle bundle = new Bundle();
		bundle.putString("resultCode", resultCode);
		bundle.putString("resultDetail", resultDetail);
		bundle.putString("apkFilePath", ApkFileActivity.apkFilePath);
		bundle.putString("apkFileName", ApkFileActivity.apkFileName);
		bundle.putString("versionCode", String.valueOf(ApkFileActivity.versionCode));
		intent.putExtra("apkBitmap", ApkFileActivity.apkBitmap);
		
		if(PRAPVerifyAsyncTask.status.equals("uninstall")||!PRAPVerifyAsyncTask.dbcheck.equals("true")){
			bundle.putString("status", PRAPVerifyAsyncTask.status);
			bundle.putString("dbcheck", PRAPVerifyAsyncTask.dbcheck);
		}else{
			bundle.putString("status", PRAPVerifyAsyncTask.status);
			bundle.putString("dbcheck", PRAPVerifyAsyncTask.dbcheck);
			bundle.putString("privacyRiskShow", PRAPVerifyAsyncTask.AnalysisResult.toString());
			bundle.putString("toastRisk", PRAPVerifyAsyncTask.toastStSpannableString.toString());
			bundle.putInt("showVar", PRAPVerifyAsyncTask.showVar);		
			bundle.putInt("apkposition", PRAPVerifyAsyncTask.apkposition);
			bundle.putDouble("privacyRisk", PRAPVerifyAsyncTask.privacyRisk);
			bundle.putStringArrayList("tableNameList", PRAPVerifyAsyncTask.tableNameList);
			bundle.putStringArrayList("columnNameList", PRAPVerifyAsyncTask.columnNameList);
			bundle.putStringArrayList("acPrivcyData", PRAPVerifyAsyncTask.acPrivcyData);
			bundle.putStringArrayList("ac1PrivcyData", PRAPVerifyAsyncTask.ac1PrivcyData);
			bundle.putStringArrayList("ac2PrivcyData", PRAPVerifyAsyncTask.ac2PrivcyData);
			bundle.putStringArrayList("ipPrivcyData", PRAPVerifyAsyncTask.ipPrivcyData);
			bundle.putStringArrayList("ip1PrivcyData", PRAPVerifyAsyncTask.ip1PrivcyData);
			bundle.putStringArrayList("ip2PrivcyData", PRAPVerifyAsyncTask.ip2PrivcyData);
			bundle.putStringArrayList("databaseName", PRAPVerifyAsyncTask.databaseName);			
			intent.putExtra("apkBitmap", ApkFileActivity.apkBitmap);
			intent.putExtra("PLbetaList", PRAPVerifyAsyncTask.PLbetaList);
			intent.putExtra("SortPLbetaList", PRAPVerifyAsyncTask.SortPLbetaList);
			intent.putExtra("filePlNonZero", PRAPVerifyAsyncTask.filePlNonZero);
		}
		
		intent.putExtras(bundle);
		mContext.startActivity(intent);
//		((Activity)mContext).finish();

	}
}