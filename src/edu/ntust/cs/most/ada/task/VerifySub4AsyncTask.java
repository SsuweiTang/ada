package edu.ntust.cs.most.ada.task;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import edu.ntust.cs.most.ada.activity.ApkFileSub4Activity;
import edu.ntust.cs.most.ada.activity.DetailResultSub4Activity;
import edu.ntust.cs.most.ada.activity.ResultActivity;
import edu.ntust.cs.most.ada.domain.APKManifest;
import edu.ntust.cs.most.ada.modelImpl.APKInfoExtractorImpl;
import edu.ntust.cs.most.ada.parameter.ServiceURL;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

/**
 * 
 * @author Ssu-Wei,Tang
 *
 */
public class VerifySub4AsyncTask extends AsyncTask<Void, Integer, String> {
	private APKManifest apkManifest;
	private MultiValueMap<String, Object> map;
	private HttpHeaders headers;
	private String resultCode;
	private String resultDetail;
	private Context mContext;

	public VerifySub4AsyncTask(Context context) {

		mContext = context;		
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();		
		
		ApkFileSub4Activity.showProgressDialog();		
		APKInfoExtractorImpl apkInfoExtractorService = new APKInfoExtractorImpl();
		apkManifest = apkInfoExtractorService.extractManifestInfo(
				ApkFileSub4Activity.apkFilePath, ApkFileSub4Activity.apkFileName,
				ApkFileSub4Activity.versionCode);
		map = new LinkedMultiValueMap<String, Object>();
		map.add("file", new FileSystemResource(new File(
				ApkFileSub4Activity.apkFilePath)));
		map.add("filename", ApkFileSub4Activity.apkFileName);
		map.add("androidManifestXml", apkManifest.getShaForAndroidManifestXml());
		map.add("resourcesArsc", apkManifest.getShaForResourcesArsc());
		map.add("classesDex", apkManifest.getShaForClassesDex());
		map.add("mnftEntrySize",
				Integer.toString(apkManifest.getMnftEntrySize()));
		map.add("versionCode", apkManifest.getVersionCode());
		headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);	
		
		
		System.out.println(ApkFileSub4Activity.apkFileName);
		System.out.println(apkManifest.getShaForAndroidManifestXml());
		System.out.println(apkManifest.getShaForResourcesArsc());
		System.out.println(apkManifest.getShaForClassesDex());
		System.out.println(apkManifest.getMnftEntrySize());
		System.out.println(apkManifest.getVersionCode());
		
	}

	@Override
	protected String doInBackground(Void... arg0) {
		ResponseEntity<String> response = null;
		try {
			RestTemplate restTemplate = new RestTemplate(true);
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
			requestFactory.setConnectTimeout(4000);
			restTemplate.setRequestFactory(requestFactory);
			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(
					map, headers);
			try {
				response = restTemplate.exchange(
						ServiceURL.SHA1ANDFINGERPRINT_VERIFICATION,
						HttpMethod.POST, requestEntity, String.class);
				if (response.getStatusCode().equals(HttpStatus.OK))
					return response.getBody();
			} catch (HttpClientErrorException e) {
				Log.e("Network Error ", e.getLocalizedMessage(), e);
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		String warm = "";
		try {
			JSONObject jsonResponse = null;

			if (result == null) {
				resultCode = "0";
			} else {
				jsonResponse = new JSONObject(result);
				if (jsonResponse.getString("upload").equals("success")) {
					if (jsonResponse.getInt("verifyResult") == 1) {
						resultCode = "1";
					} else if (jsonResponse.getInt("verifyResult") == 3) {
						resultCode = "3";
					} else if (jsonResponse.getInt("verifyResult") == 2) {
						resultCode = "2";
					} else if (jsonResponse.getInt("verifyResult") == 4) {
						resultCode = "4";
						String[] AfterSplitError = jsonResponse.getString(
								"verifyError").split(",");
						for (int i = 0; i < AfterSplitError.length; i++) {
							warm +=" * "+ AfterSplitError[i] + "\n";
						}
						warm = warm.substring(0, warm.length() - 1);
						resultDetail = warm;

					}
				} else {
					resultCode = "5";

				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
	
		
		ApkFileSub4Activity.progressDialog.dismiss();
		
		jumpToResult();
		
		
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();

	}

	
	
	
	protected void jumpToResult() {
		Intent intent = new Intent();
		intent.setClass(mContext, DetailResultSub4Activity.class);
		Bundle bundle = new Bundle();
		bundle.putString("resultCode", resultCode);
		bundle.putString("resultDetail", resultDetail);
		bundle.putString("apkFilePath", ApkFileSub4Activity.apkFilePath);
		bundle.putString("apkFileName", ApkFileSub4Activity.apkFileName);
		bundle.putString("versionCode", String.valueOf(ApkFileSub4Activity.versionCode));
		intent.putExtra("apkBitmap", ApkFileSub4Activity.apkBitmap);
	
		
		intent.putExtras(bundle);
		mContext.startActivity(intent);
		((Activity)mContext).finish();

	}
}