package edu.ntust.cs.most.ada.task;

import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.jar.JarFile;

import org.xmlpull.v1.XmlPullParser;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;
import edu.ntust.cs.most.ada.R;
import edu.ntust.cs.most.ada.activity.ApkFileSub3Activity;
import edu.ntust.cs.most.ada.activity.ApkFileSub5Activity;
import edu.ntust.cs.most.ada.activity.AppInfo;
import edu.ntust.cs.most.ada.activity.DetailResultSub3Activity;
import edu.ntust.cs.most.ada.activity.ResultActivity;
import edu.ntust.cs.most.ada.activity.SystemPackageTools;

public class PRAPVerifyAsyncTask extends AsyncTask<Void, Integer, String> {

	private Context mContext;
	
	Process process;
	Cursor C;
	private String[] Filename = null;
	public static String status = "uninstall";
	
	final File copyFilePath = new File("/data/data");
	final File pasteFilePath = new File("/sdcard/Download");
	
	public static ArrayList<String> tableNameList = new ArrayList<String>();
	public static ArrayList<String> columnNameList = new ArrayList<String>();
	public static ArrayList<String> acPrivcyData = new ArrayList<String>();
	public static ArrayList<String> ac1PrivcyData = new ArrayList<String>();
	public static ArrayList<String> ac2PrivcyData = new ArrayList<String>();
	public static ArrayList<String> ipPrivcyData = new ArrayList<String>();
	public static ArrayList<String> ip1PrivcyData = new ArrayList<String>();
	public static ArrayList<String> ip2PrivcyData = new ArrayList<String>();
	public static ArrayList<String> databaseName = new ArrayList<String>(Arrays.asList("",
			"", "", ""));
	public static ArrayList<Double> PLbetaList = new ArrayList<Double>();
	public static ArrayList<Double> SortPLbetaList = new ArrayList<Double>();
	public static ArrayList<Integer> filePlNonZero = new ArrayList<Integer>();
	
	public static int risklen = 0;
	public static int risklen2 = 0;
	public static int toastlen = 0;

	String analysisDbFilePath = null;

	public static SpannableString AnalysisResult = null;
	public static SpannableString toastStSpannableString = null;

	public static int apkposition;

	public static double privacyRisk = 0.0;
	double alpha = 0.5, beta = 0.5;
	public static int showVar = 0;
	
	public static String dbcheck = "true";
	public static int finish = 0;

	public PRAPVerifyAsyncTask(Context context) {

		mContext = context;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	
		status = "uninstall";
		dbcheck = "true";
		privacyRisk = 0.0;
		finish = 0;
		ApkFileSub3Activity.showProgressDialog();
	}

	@Override
	protected String doInBackground(Void... arg0) {
		
		try {
			getRoot();
			getPackageName(ApkFileSub3Activity.apkFilePath);
			queryAppInfo();
			privacyRisk = 0;
			databaseName = new ArrayList<String>(Arrays.asList("", "", "",
					""));
			
			for(AppInfo content:ApkFileSub3Activity.mlistAppInfo){
				if (content.getPkgName().contentEquals(ApkFileSub3Activity.apkPackageName)){
					showVar = 1;
					apkposition = ApkFileSub3Activity.mlistAppInfo.indexOf(content);
					capturingModule(apkposition);
					status = "installed";
					break;					
				}
			}
			
			while(status.equals("installed")){
				if(finish==1){
					if (showVar == 1) {
						AnalysisResult = new SpannableString(mContext.getResources().getString(
								R.string.sub3_verifyResult_Privacy_Risk)+"=");
						risklen = AnalysisResult.length();
						AnalysisResult = new SpannableString(
								AnalysisResult + ""	+ privacyRisk);
						risklen2 = AnalysisResult.length();
						if (privacyRisk > 0.5) {
							toastStSpannableString = new SpannableString(mContext.getResources().getString(
									R.string.sub3_verifyResult_Risk_high));
							toastlen = toastStSpannableString
									.length();
														
						} else if (0.5 > privacyRisk
								&& privacyRisk > 0.25) {
							toastStSpannableString = new SpannableString(mContext.getResources().getString(
									R.string.sub3_verifyResult_Risk_mid));
							toastlen = toastStSpannableString
									.length();
														
						} else if (0.25 > privacyRisk) {
							
							toastStSpannableString = new SpannableString(mContext.getResources().getString(
									R.string.sub3_verifyResult_Risk_low));
							toastlen = toastStSpannableString
									.length();
																			
						}
						break;
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		ApkFileSub3Activity.progressDialog.dismiss();
		jumpToResult();	
		


	}

	@Override
	protected void onCancelled() {
		super.onCancelled();

	}

	public void capturingModule(int position) {

		// 彈出Analysis Dialog
//		if (!(appSpinner.getSelectedItemPosition() == 0)) {

//			final CharSequence strDialogTitle = "Loading";
//			final CharSequence strDialogBody = "Analysis "
//					+ ApkFileActivity.mlistAppInfo.get(position).getAppLabel()
//					+ " please wait！";
//			PDialog = ProgressDialog.show(CapturingModule.this, strDialogTitle,
//					strDialogBody, true);

			new Thread() {
				public void run() {
					try {
						sleep(3000);
						Overall();
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
//						PDialog.dismiss();
					}
				}
			}.start();

			analysisDbFilePath = pasteFilePath + File.separator + "copyDB"
					+ File.separator + ApkFileSub3Activity.mlistAppInfo.get(position).getPkgName()
					+ File.separator;

			// 顯示Progress對話方塊
//			Toast.makeText(
//					CapturingModule.this,
//					"You select the "
//							+ ApkFileActivity.mlistAppInfo.get(position).getAppLabel(),
//					Toast.LENGTH_LONG).show();
			
			copyFolderUseCmd(
					copyFilePath + File.separator
							+ ApkFileSub3Activity.mlistAppInfo.get(position).getPkgName()
							+ File.separator + "databases" + File.separator,
					analysisDbFilePath);
			
			File dbFile = new File(pasteFilePath + File.separator + "copyDB"
					+ File.separator + ApkFileSub3Activity.mlistAppInfo.get(position).getPkgName()
					+ File.separator);

			Filename = dbFile.list();
			Arrays.sort(Filename);

			if (Filename.length == 0) {
				dbcheck = "this app no such .db file...";
				
			}
			
//			showDbList();

//		}
	}
	@SuppressLint("NewApi")
	public void Overall() {

		int plLen = 0;
		int highPlBeta = 0;
		double highGamma = 0.9;
		double lowGamma = 1 - highGamma;
		// 分析每個SQLite file
		PLbetaList = new ArrayList<Double>();
		SortPLbetaList = new ArrayList<Double>();
		for (int fileNo = 0; fileNo < Filename.length; fileNo++) {

			double acRe = 0;
			double acPr = 0;
			double acFmeasure = 0;
			double ipRe = 0;
			double ipPr = 0;
			double ipFmeasure = 0;

			tableNameList.clear();

			acPrivcyData = new ArrayList<String>(Arrays.asList("", "", "", "",
					"", "", "", ""));
			ipPrivcyData = new ArrayList<String>(Arrays.asList("", "", "", "",
					"", "", "", ""));
			SQLiteDatabase db = SQLiteDatabase.openDatabase(analysisDbFilePath
					+ Filename[fileNo], null, 0);
			C = db.rawQuery(
					"SELECT tbl_name FROM sqlite_master GROUP BY tbl_name",
					null);

			while (C.moveToNext()) {

				for (int x = 0; x < C.getColumnCount(); x++) {

					String tableName = C.getString(x);
					tableNameList.add(tableName);
				}
			}
			C.close();
			String columnValues = null;
			for (int tableNo = 0; tableNo < tableNameList.size(); tableNo++) {
				columnNameList.clear();

				C = db.rawQuery(
						"PRAGMA table_info(" + tableNameList.get(tableNo) + ")",
						null);

				while (C.moveToNext()) {

					for (int i = 1; i < C.getColumnCount(); i = i + 6) {

						String columnName = C.getString(i);
						columnNameList.add(columnName);
					}
				}
				C.close();
				for (int y = 0; y < columnNameList.size(); y++) {

					for (int ac = 0; ac < ApkFileSub3Activity.attackCriteria.length; ac++) {
						// columnValues = null;
						// System.out.println("column size "+columnNameList.size());
						// columnValuesList.clear();
						C = db.rawQuery("SELECT " + columnNameList.get(y)
								+ " FROM " + tableNameList.get(tableNo)
								+ " WHERE " + columnNameList.get(y)
								+ " Like '%" + ApkFileSub3Activity.attackCriteria[ac]
								+ "%' group by " + columnNameList.get(y), null);

						while (C.moveToNext()) {

							if (C.getType(0) != 4) {
								columnValues = String.valueOf(C.getString(0));
							}
						}
						C.close();

						switch (ac) {
						case 0:
							if (columnValues != null) {

								acPrivcyData.set(0, ApkFileSub3Activity.attackCriteria[0]);
								columnValues = null;
							}
							break;
						case 1:
							if (columnValues != null) {

								acPrivcyData.set(1, ApkFileSub3Activity.attackCriteria[1]);
								columnValues = null;
							}
							break;
						case 2:
							if (columnValues != null) {

								acPrivcyData.set(2, ApkFileSub3Activity.attackCriteria[2]);
								columnValues = null;
							}
							break;
						case 3:
							if (columnValues != null) {

								acPrivcyData.set(3, ApkFileSub3Activity.attackCriteria[3]);
								columnValues = null;
							}
							break;
						case 4:
							if (columnValues != null) {

								acPrivcyData.set(4, ApkFileSub3Activity.attackCriteria[4]);
								columnValues = null;
							}
							break;
						case 5:
							if (columnValues != null) {

								acPrivcyData.set(5, ApkFileSub3Activity.attackCriteria[5]);
								columnValues = null;
							}
							break;
						case 6:
							if (columnValues != null) {

								acPrivcyData.set(6, ApkFileSub3Activity.attackCriteria[6]);
								columnValues = null;
							}
							break;
						case 7:
							if (columnValues != null) {

								acPrivcyData.set(7, ApkFileSub3Activity.attackCriteria[7]);
								columnValues = null;
							}
							break;
						}

					}

					for (int ip = 0; ip < ApkFileSub3Activity.individualPerception.length; ip++) {

						C = db.rawQuery("SELECT " + columnNameList.get(y)
								+ " FROM " + tableNameList.get(tableNo)
								+ " WHERE " + columnNameList.get(y)
								+ " Like '%" + ApkFileSub3Activity.individualPerception[ip]
								+ "%' group by " + columnNameList.get(y), null);

						while (C.moveToNext()) {

							if (C.getType(0) != 4) {
								columnValues = String.valueOf(C.getString(0));
							}
						}
						C.close();

						switch (ip) {
						case 0:
							if (columnValues != null) {

								ipPrivcyData.set(0, ApkFileSub3Activity.individualPerception[0]);
								columnValues = null;
							}
							break;
						case 1:
							if (columnValues != null) {

								ipPrivcyData.set(1, ApkFileSub3Activity.individualPerception[1]);
								columnValues = null;
							}
							break;
						case 2:
							if (columnValues != null) {

								ipPrivcyData.set(2, ApkFileSub3Activity.individualPerception[2]);
								columnValues = null;
							}
							break;
						case 3:
							if (columnValues != null) {

								ipPrivcyData.set(3, ApkFileSub3Activity.individualPerception[3]);
								columnValues = null;
							}
							break;
						case 4:
							if (columnValues != null) {

								ipPrivcyData.set(4, ApkFileSub3Activity.individualPerception[4]);
								columnValues = null;
							}
							break;
						case 5:
							if (columnValues != null) {

								ipPrivcyData.set(5, ApkFileSub3Activity.individualPerception[5]);
								columnValues = null;
							}
							break;
						case 6:
							if (columnValues != null) {

								ipPrivcyData.set(6, ApkFileSub3Activity.individualPerception[6]);
								columnValues = null;
							}
							break;
						case 7:
							if (columnValues != null) {

								ipPrivcyData.set(7, ApkFileSub3Activity.individualPerception[7]);
								columnValues = null;
							}
							break;
						}
					}
				}
			}

			acRe = 0;
			acPr = 0;
			acFmeasure = 0;
			int wNameAc = 2, wPhoneAc = 3, wAddAc = 3, wEmailAc = 2, wPasswordAc = 4, wContentAc = 3, wIdAc = 2, wBdayAc = 1;
			int acReD = wNameAc + wPhoneAc + wAddAc + wEmailAc + wPasswordAc
					+ wContentAc + wIdAc + wBdayAc;

			if (acPrivcyData.get(0) == "") {

				wNameAc = 0;
			} else {

				wNameAc = 2;
			}
			if (acPrivcyData.get(1) == "") {

				wPhoneAc = 0;
			} else {

				wPhoneAc = 3;
			}
			if (acPrivcyData.get(2) == "") {

				wAddAc = 0;
			} else {

				wAddAc = 3;
			}
			if (acPrivcyData.get(3) == "") {

				wEmailAc = 0;
			} else {

				wEmailAc = 2;
			}
			if (acPrivcyData.get(4) == "") {

				wPasswordAc = 0;
			} else {

				wPasswordAc = 4;
			}
			if (acPrivcyData.get(5) == "") {

				wContentAc = 0;
			} else {

				wContentAc = 3;
			}
			if (acPrivcyData.get(6) == "") {

				wIdAc = 0;
			} else {

				wIdAc = 2;
			}
			if (acPrivcyData.get(7) == "") {

				wBdayAc = 0;
			} else {

				wBdayAc = 1;
			}

			if ((wNameAc + wPhoneAc + wAddAc + wEmailAc + wPasswordAc
					+ wContentAc + wIdAc + wBdayAc) != 0) {

				acPr = (double) (wNameAc + wPhoneAc + wAddAc + wEmailAc
						+ wPasswordAc + wContentAc + wIdAc + wBdayAc)
						/ (double) (wNameAc + wPhoneAc + wAddAc + wEmailAc
								+ wPasswordAc + wContentAc + wIdAc + wBdayAc);
				acPr = (int) (Math.round(acPr * 100.0)) / 100.0;
				acRe = (double) (wNameAc + wPhoneAc + wAddAc + wEmailAc
						+ wPasswordAc + wContentAc + wIdAc + wBdayAc)
						/ (double) acReD;
				acRe = (int) (Math.round(acRe * 100.0)) / 100.0;
				acFmeasure = (double) acPr * acRe
						/ (double) ((alpha * acRe) + ((1 - alpha) * acPr));
				acFmeasure = (int) (Math.round(acFmeasure * 100.0)) / 100.0;

			} else {

				acPr = 0;
				acRe = 0;
				acFmeasure = 0;
			}

			ipRe = 0;
			ipPr = 0;
			ipFmeasure = 0;

//			Bundle cmBundle = mContext.getIntent().getExtras();
			int wNameIp = ApkFileSub3Activity.nameWeight, wPhoneIp = ApkFileSub3Activity.phoneWeight, wAddIp = ApkFileSub3Activity.cityWeight, wEmailIp = ApkFileSub3Activity.mailWeight, wPasswordIp = ApkFileSub3Activity.passwordWeight, wContentIp = ApkFileSub3Activity.contentWeight, wIdIp = ApkFileSub3Activity.idWeight, wBdayIp = ApkFileSub3Activity.bdayWeight;
			// int wNameIp = 2, wPhoneIp = 3, wAddIp = 3, wEmailIp = 2,
			// wPasswordIp = 4, wContentIp = 3, wIdIp = 2;

			int ipReD = wNameIp + wPhoneIp + wAddIp + wEmailIp + wPasswordIp
					+ wContentIp + wIdIp;
			
			if (ipPrivcyData.get(0) == "") {

				wNameIp = 0;
			} else {

				wNameIp = 2;
			}
			if (ipPrivcyData.get(1) == "") {

				wPhoneIp = 0;
			} else {

				wPhoneIp = 3;
			}
			if (ipPrivcyData.get(2) == "") {

				wAddIp = 0;
			} else {

				wAddIp = 3;
			}
			if (ipPrivcyData.get(3) == "") {

				wEmailIp = 0;
			} else {

				wEmailIp = 2;
			}
			if (ipPrivcyData.get(4) == "") {

				wPasswordIp = 0;
			} else {

				wPasswordIp = 4;
			}
			if (ipPrivcyData.get(5) == "") {

				wContentIp = 0;
			} else {

				wContentIp = 3;
			}
			if (ipPrivcyData.get(6) == "") {

				wIdIp = 0;
			} else {

				wIdIp = 2;
			}
			if (ipPrivcyData.get(7) == "") {

				wBdayIp = 0;
			} else {

				wBdayIp = 2;
			}

			if ((wNameIp + wPhoneIp + wAddIp + wEmailIp + wPasswordIp
					+ wContentIp + wIdIp) != 0) {

				ipPr = (double) (wNameIp + wPhoneIp + wAddIp + wEmailIp
						+ wPasswordIp + wContentIp + wIdIp + wBdayIp)
						/ (double) (wNameIp + wPhoneIp + wAddIp + wEmailIp
								+ wPasswordIp + wContentIp + wIdIp + wBdayIp);
				ipPr = (int) (Math.round(ipPr * 100.0)) / 100.0;

				ipRe = (double) (wNameIp + wPhoneIp + wAddIp + wEmailIp
						+ wPasswordIp + wContentIp + wIdIp + wBdayIp)
						/ (double) ipReD;
				ipRe = (int) (Math.round(ipRe * 100.0)) / 100.0;

				ipFmeasure = (double) ipPr * ipRe
						/ (double) ((alpha * ipRe) + ((1 - alpha) * ipPr));
				ipFmeasure = (int) (Math.round(ipFmeasure * 100.0)) / 100.0;
			} else {

				ipPr = 0;
				ipRe = 0;
				ipFmeasure = 0;
			}
			ipFmeasure = beta * acFmeasure + (1 - beta) * ipFmeasure;
			ipFmeasure = (int) (Math.round(ipFmeasure * 100.0)) / 100.0;
			PLbetaList.add(ipFmeasure);

		}

		for (int i = 0; i < PLbetaList.size(); i++) {
			SortPLbetaList.add(i, PLbetaList.get(i));
		}

		for (int i = 0; i < PLbetaList.size(); i++) {
			if (PLbetaList.get(i) != 0.0) {
				filePlNonZero.add(i);
			}
		}

		Collections.sort(SortPLbetaList);
		Collections.reverse(SortPLbetaList);
		System.out.println("PLbetaList" + PLbetaList);
		System.out.println("SortPLbetaList" + SortPLbetaList);

		for (int i = 0; i < SortPLbetaList.size(); i++) {
			if (SortPLbetaList.get(i) != 0) {
				plLen++;
			}
			if (SortPLbetaList.get(i) >= 0.5) {
				highPlBeta++;
			}
		}

		System.out.println("SortPLbetaList" + SortPLbetaList + "\n" + plLen
				+ "\n" + highPlBeta);

		if (highPlBeta == 0 && plLen == 0) {
			privacyRisk = 0;
		} else if (highPlBeta == 0) {
			lowGamma = 1;
			for (int i = 0; i < plLen; i++) {
				SortPLbetaList.set(i,
						(lowGamma / plLen) / SortPLbetaList.get(i));
			}
			for (int i = 0; i < plLen; i++) {
				privacyRisk = privacyRisk + SortPLbetaList.get(i);
			}
			privacyRisk = 1 / privacyRisk;
			privacyRisk = (int) (Math.round(privacyRisk * 100.0)) / 100.0;
		} else if (plLen == 0) {
			privacyRisk = 0;
		} else {
			for (int i = 0; i < highPlBeta; i++) {
				SortPLbetaList.set(i,
						(highGamma / highPlBeta) / SortPLbetaList.get(i));
			}
			for (int i = plLen - highPlBeta; i < plLen; i++) {
				SortPLbetaList.set(i, (lowGamma / (plLen - highPlBeta))
						/ SortPLbetaList.get(i));
			}
			for (int i = 0; i < plLen; i++) {
				privacyRisk = privacyRisk + SortPLbetaList.get(i);
			}
			privacyRisk = 1 / privacyRisk;
			privacyRisk = (int) (Math.round(privacyRisk * 100.0)) / 100.0;
		}
		System.out.println("SortPLbetaList" + SortPLbetaList + "\n" + plLen
				+ "\n" + highPlBeta + "\nPrivacyRisk = " + privacyRisk);
		finish = 1;
	}
	
	public void copyFolderUseCmd(String oldPath, String newPath) {

		try {
			(new File(newPath)).mkdirs(); // 如果資料夾不存在則建立資料夾
//			modifyFile(new File(oldPath));
//			File a = new File(oldPath);			
//			String[] file = a.list();
			File temp = null;

			process = Runtime.getRuntime().exec("su");
			DataOutputStream cm = new DataOutputStream(
					process.getOutputStream());
			
			cm.writeBytes("chmod 777 " + oldPath + "\n");
			File a = new File(oldPath);			
			String[] file = a.list();

			for (int i = 0; i < file.length; i++) {
				System.out.println("in");
				if (oldPath.endsWith(File.separator)) {
					temp = new File(oldPath + file[i]);
				} else {
					temp = new File(oldPath + File.separator + file[i]);
				}

				if (temp.isFile()) {

					if (!file[i].endsWith("-journal")
							&& !file[i].endsWith("-uid")) {

						cm.writeBytes("cp ~" + temp + " " + newPath
								+ File.separator + file[i] + "\n");
					}
				}

				// 如果是子資料夾
				if (temp.isDirectory()) {
					copyFolderUseCmd(oldPath + "/" + file[i], newPath + "/"
							+ file[i]);
				}
			}

			cm.writeBytes("exit\n");
			cm.flush();
			process.waitFor();

		} catch (Exception e) {

			Log.d("copyFolderUseCmd Error", "未複製成功..." + e.getMessage());
		}
	}
	
	public static void getPackageName(String apkFilePath) {

	    // Decompress found APK's Manifest XML
	    // Source: http://stackoverflow.com/questions/2097813/how-to-parse-the-androidmanifest-xml-file-inside-an-apk-package/4761689#4761689
	    try {

	        if ((new File(apkFilePath).exists())) {

	            JarFile jf = new JarFile(apkFilePath);
	            InputStream is = jf.getInputStream(jf.getEntry("AndroidManifest.xml"));
	            byte[] xml = new byte[is.available()];
	            int br = is.read(xml);

	            //Tree tr = TrunkFactory.newTree();
	            String xmlResult = SystemPackageTools.decompressXML(xml);
	            //prt("XML\n"+tr.list());

	            if (!xmlResult.isEmpty()) {

	                InputStream in = new ByteArrayInputStream(xmlResult.getBytes());

	                // Source: http://developer.android.com/training/basics/network-ops/xml.html
	                XmlPullParser parser = Xml.newPullParser();
	                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

	                parser.setInput(in, null);
	                parser.nextTag();

	                String name = parser.getName();
	                if (name.equalsIgnoreCase("Manifest")) {

	                	ApkFileSub3Activity.apkPackageName = parser.getAttributeValue(null, "package");	                            
	                }

	            }
	        }

	    } catch (Exception ex) {
//	        android.util.Log.e(TAG, "getIntents, ex: "+ex);
	        ex.printStackTrace();
	    }

	}
	
	public void queryAppInfo() {

		// 獲得PackageManager對象
		PackageManager pm = mContext.getPackageManager();
		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		// 通過查詢，獲得所有ResolveInfo對象
		List<ResolveInfo> resolveInfos = pm
				.queryIntentActivities(mainIntent, 0);

		// 調用系統排序，根據name排序
		// 該排序很重要，否則只能顯示系統應用，而不能列出第三方應用程式
		Collections.sort(resolveInfos,
				new ResolveInfo.DisplayNameComparator(pm));
		if (ApkFileSub3Activity.mlistAppInfo != null) {
			ApkFileSub3Activity.mlistAppInfo.clear();

//			AppInfo DefaultSelect = new AppInfo();
//			DefaultSelect.setAppLabel("Click here to select the application!!");
//			mlistAppInfo.add(DefaultSelect);
			for (ResolveInfo reInfo : resolveInfos) {
				// 獲得應用程式的自動Activity的name
				String activityName = reInfo.activityInfo.name;
				// 獲得應用程式的PakageName
				String pkgName = reInfo.activityInfo.packageName;

				// 獲得應用程式的Label
				String appLabel = (String) reInfo.loadLabel(pm);
				// 獲得應用程式的圖示
				Drawable icon = reInfo.loadIcon(pm);
				// 為應用程式的啟動Activity準備Intent
				Intent launchIntent = new Intent();
				launchIntent.setComponent(new ComponentName(pkgName,
						activityName));
				// 創建一個AppInfo，並創建一個AppInfo對象，並賦值
				AppInfo appInfo = new AppInfo();
				appInfo.setAppLabel(appLabel);
				appInfo.setPkgName(pkgName);
				appInfo.setAppIcon(icon);
				appInfo.setIntent(launchIntent);
				// 添加至列表中
				ApkFileSub3Activity.mlistAppInfo.add(appInfo);				
			}
		}
	}
	public void getRoot() {
		try {

			Process process = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(
					process.getOutputStream());
			Thread.sleep(3000);
			os.writeBytes("mount -oremount,rw /dev/block/mtdblock3 /system\n");
			os.writeBytes("busybox chown 0:0 /system/xbin/su\n");
//			os.writeBytes("find /data/data/com.facebook.katana -type d -print0 |xargs -0 chmod 777\n");
			os.writeBytes("exit\n");
			os.flush();
			process.waitFor();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e("getRoot", e.getMessage());
		}
	}
	
	
	protected void jumpToResult() {
		Intent intent = new Intent();
		intent.setClass(mContext, DetailResultSub3Activity.class);
		Bundle bundle = new Bundle();
		bundle.putString("apkFilePath", ApkFileSub3Activity.apkFilePath);
		bundle.putString("apkFileName", ApkFileSub3Activity.apkFileName);
		bundle.putString("versionCode", String.valueOf(ApkFileSub3Activity.versionCode));
		intent.putExtra("apkBitmap", ApkFileSub3Activity.apkBitmap);
		
		if(PRAPVerifyAsyncTask.status.equals("uninstall")||!dbcheck.equals("true")){
			bundle.putString("status", status);
			bundle.putString("dbcheck", dbcheck);
		}else{
			bundle.putString("status", status);
			bundle.putString("dbcheck", dbcheck);
			bundle.putString("privacyRiskShow", AnalysisResult.toString());
			bundle.putString("toastRisk", toastStSpannableString.toString());
			bundle.putInt("showVar", showVar);		
			bundle.putInt("apkposition", apkposition);
			bundle.putDouble("privacyRisk", privacyRisk);
			bundle.putStringArrayList("tableNameList", tableNameList);
			bundle.putStringArrayList("columnNameList", columnNameList);
			bundle.putStringArrayList("acPrivcyData", acPrivcyData);
			bundle.putStringArrayList("ac1PrivcyData", ac1PrivcyData);
			bundle.putStringArrayList("ac2PrivcyData", ac2PrivcyData);
			bundle.putStringArrayList("ipPrivcyData", ipPrivcyData);
			bundle.putStringArrayList("ip1PrivcyData", ip1PrivcyData);
			bundle.putStringArrayList("ip2PrivcyData", ip2PrivcyData);
			bundle.putStringArrayList("databaseName", databaseName);			
			intent.putExtra("apkBitmap", ApkFileSub3Activity.apkBitmap);
			intent.putExtra("PLbetaList", PRAPVerifyAsyncTask.PLbetaList);
			intent.putExtra("SortPLbetaList", PRAPVerifyAsyncTask.SortPLbetaList);
			intent.putExtra("filePlNonZero", PRAPVerifyAsyncTask.filePlNonZero);
		}
		
		intent.putExtras(bundle);
		mContext.startActivity(intent);
		((Activity)mContext).finish();

	}
}
