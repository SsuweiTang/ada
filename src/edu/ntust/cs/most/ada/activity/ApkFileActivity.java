package edu.ntust.cs.most.ada.activity;

import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.jar.JarFile;

import org.xmlpull.v1.XmlPullParser;

import edu.ntust.cs.most.ada.domain.APKManifest;
import edu.ntust.cs.most.ada.task.AMDRVVerifyAsyncTask;
import edu.ntust.cs.most.ada.task.PRAPVerifyAsyncTask;
import edu.ntust.cs.most.ada.task.VerifyAsyncTask;
import edu.ntust.cs.most.ada.util.APKFileHandler;
import edu.ntust.cs.most.ada.util.ImageTransformer;
import edu.ntust.cs.most.ada.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

/**
 * 
 * @author Ssu-Wei,Tang
 *
 */
public class ApkFileActivity extends Activity {

	private TextView textViewApkFileName;
	private ImageView imageViewApkIcon;
	private static final int REQUEST_PATH = 1;
	public static String apkFileName;
	public static String apkFilePath;
	public static int versionCode;
	public static Bitmap apkBitmap;
	public int s = 0;
	private APKManifest apkManifest;

	public static ProgressDialog progressDialog;
	private WifiManager wifiManager;
	private DisplayMetrics metrics;
	public static Context mContext;

	private ImageView imageViewFolder;
	public static Button buttonVerify;
	public static ImageButton buttonSearch;
//	public EditText editTextApkFile;
    private String[] applicationName;
    private AutoCompleteTextView autoCompleteTextViewAppName;

	public static int nameWeight, phoneWeight, cityWeight, mailWeight,
			passwordWeight, contentWeight, idWeight, bdayWeight;
	public static String apkPackageName;
	TextView nameWvalue, phoneWvalue, cityWvalue, bdayWvalue, mailWvalue,
			passwordWvalue, contentWvalue, idWvalue;
	EditText edName, edPhone, edCity, edBday, edMail, edPassword, edContent,
			edId;
	CheckBox nameC, phoneC, cityC, bdayC, mailC, passwordC, contentC, idC;
	SeekBar nameW, phoneW, cityW, bdayW, mailW, passwordW, contentW, idW;
	public static String[] attackCriteria = new String[8];
	public static String[] individualPerception = new String[8];

	public static List<AppInfo> mlistAppInfo = null;
	
	public static PRAPVerifyAsyncTask  pravTask=null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_apkfile);

		mContext = this;
		findView();
		setListeners();

		mlistAppInfo = new ArrayList<AppInfo>();
		edName = (EditText) findViewById(R.id.EditText1);
		edPhone = (EditText) findViewById(R.id.EditText2);
		edCity = (EditText) findViewById(R.id.EditText3);
		edBday = (EditText) findViewById(R.id.EditText4);
		edMail = (EditText) findViewById(R.id.EditText5);
		edPassword = (EditText) findViewById(R.id.EditText6);
		edContent = (EditText) findViewById(R.id.EditText7);
		edId = (EditText) findViewById(R.id.EditText8);
		nameWvalue = (TextView) findViewById(R.id.Weighttext1);
		phoneWvalue = (TextView) findViewById(R.id.Weighttext2);
		cityWvalue = (TextView) findViewById(R.id.Weighttext3);
		bdayWvalue = (TextView) findViewById(R.id.Weighttext4);
		mailWvalue = (TextView) findViewById(R.id.Weighttext5);
		passwordWvalue = (TextView) findViewById(R.id.Weighttext6);
		contentWvalue = (TextView) findViewById(R.id.Weighttext7);
		idWvalue = (TextView) findViewById(R.id.Weighttext8);
		nameC = (CheckBox) findViewById(R.id.CheckBox1);
		phoneC = (CheckBox) findViewById(R.id.CheckBox2);
		cityC = (CheckBox) findViewById(R.id.CheckBox3);
		bdayC = (CheckBox) findViewById(R.id.CheckBox4);
		mailC = (CheckBox) findViewById(R.id.CheckBox5);
		passwordC = (CheckBox) findViewById(R.id.CheckBox6);
		contentC = (CheckBox) findViewById(R.id.CheckBox7);
		idC = (CheckBox) findViewById(R.id.CheckBox8);
		nameW = (SeekBar) findViewById(R.id.SeekBar1);
		phoneW = (SeekBar) findViewById(R.id.SeekBar2);
		cityW = (SeekBar) findViewById(R.id.SeekBar3);
		bdayW = (SeekBar) findViewById(R.id.SeekBar4);
		mailW = (SeekBar) findViewById(R.id.SeekBar5);
		passwordW = (SeekBar) findViewById(R.id.SeekBar6);
		contentW = (SeekBar) findViewById(R.id.SeekBar7);
		idW = (SeekBar) findViewById(R.id.SeekBar8);

		nameW.setOnSeekBarChangeListener(SeekbarClick);
		phoneW.setOnSeekBarChangeListener(SeekbarClick);
		cityW.setOnSeekBarChangeListener(SeekbarClick);
		bdayW.setOnSeekBarChangeListener(SeekbarClick);
		mailW.setOnSeekBarChangeListener(SeekbarClick);
		passwordW.setOnSeekBarChangeListener(SeekbarClick);
		contentW.setOnSeekBarChangeListener(SeekbarClick);
		idW.setOnSeekBarChangeListener(SeekbarClick);

		nameC.setOnCheckedChangeListener(checkClick);
		phoneC.setOnCheckedChangeListener(checkClick);
		cityC.setOnCheckedChangeListener(checkClick);
		bdayC.setOnCheckedChangeListener(checkClick);
		mailC.setOnCheckedChangeListener(checkClick);
		passwordC.setOnCheckedChangeListener(checkClick);
		contentC.setOnCheckedChangeListener(checkClick);
		idC.setOnCheckedChangeListener(checkClick);

		if (!nameC.isChecked()) {
			nameW.setEnabled(false);
		}
		if (!phoneC.isChecked()) {
			phoneW.setEnabled(false);
		}

		if (!cityC.isChecked()) {
			cityW.setEnabled(false);
		}

		if (!bdayC.isChecked()) {
			bdayW.setEnabled(false);
		}

		if (!mailC.isChecked()) {
			mailW.setEnabled(false);
		}

		if (!passwordC.isChecked()) {
			passwordW.setEnabled(false);
		}

		if (!contentC.isChecked()) {
			contentW.setEnabled(false);
		}

		if (!idC.isChecked()) {
			idW.setEnabled(false);
		}

	}

	@SuppressLint("NewApi")
	private void setListeners() {

		buttonVerify.setOnClickListener(VerifyClickListener);
		imageViewApkIcon.setOnClickListener(BrowserClickListener);
		buttonSearch.setOnClickListener(SearchClickListener);

		// imageViewApkIcon.setBackground(null);
	}

	private void findView() {

		getDisplay();
		buttonSearch = (ImageButton) findViewById(R.id.buttonSearch);
//		editTextApkFile = (EditText) findViewById(R.id.editTextApkFile);

		buttonVerify = (Button) findViewById(R.id.buttonVerify);
		textViewApkFileName = (TextView) findViewById(R.id.textViewApkFileName);
		imageViewApkIcon = (ImageView) findViewById(R.id.imageViewApkIcon);
		imageViewFolder = (ImageView) findViewById(R.id.imageViewFolder);
		
		
       autoCompleteTextViewAppName=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextViewApkApplicaltionName);


		
		
		
		List<PackageInfo> appList = getAllApps(ApkFileActivity.this);
		PackageInfo packageinfo;
		PackageManager packageManager = ApkFileActivity.this
				.getPackageManager();
		
	
		applicationName=new String [appList.size()];
		
		for (int i = 0; i < appList.size(); i++) {
			packageinfo = appList.get(i);
			
			applicationName[i]=packageManager.getApplicationLabel(packageinfo.applicationInfo).toString();

		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,applicationName);
		autoCompleteTextViewAppName.setAdapter(adapter);

		setView();

	}

	private void getDisplay() {
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
	}

	public static List<PackageInfo> getAllApps(Context coNtext) {
		List<PackageInfo> apps = new ArrayList<PackageInfo>();
		PackageManager packageManager = coNtext.getPackageManager();
		List<PackageInfo> paklist = packageManager.getInstalledPackages(0);
		for (int i = 0; i < paklist.size(); i++) {
			PackageInfo pak = (PackageInfo) paklist.get(i);
			if ((pak.applicationInfo.flags & pak.applicationInfo.FLAG_SYSTEM) <= 0) {
				apps.add(pak);
			}
		}
		return apps;
	}

	@Override
	protected void onStop() {
		super.onStop();

		setView();

	}

	private Button.OnClickListener SearchClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (!autoCompleteTextViewAppName.getText().toString().equals("")) {
				PackageManager packageManager = ApkFileActivity.this
						.getPackageManager();
				List<PackageInfo> appList = getAllApps(ApkFileActivity.this);
				PackageInfo packageinfo;
				boolean found = false;
				for (int i = 0; i < appList.size(); i++) {
					packageinfo = appList.get(i);

					if (packageManager
							.getApplicationLabel(packageinfo.applicationInfo)
							.toString()
							.indexOf(autoCompleteTextViewAppName.getText().toString().trim()) == -1) {

					} else {
						found = true;
						textViewApkFileName
								.setText(packageinfo.applicationInfo.packageName
										+ ".apk");
						 apkBitmap = ImageTransformer
								.drawableToBitmap(packageManager
										.getApplicationIcon(packageinfo.applicationInfo));
						imageViewApkIcon.setImageBitmap(apkBitmap);

						try {
							ApplicationInfo AI = packageManager
									.getApplicationInfo(
											packageinfo.packageName,
											PackageManager.GET_META_DATA
													| PackageManager.GET_SHARED_LIBRARY_FILES);
							apkFilePath = AI.sourceDir;
							apkFileName = packageinfo.applicationInfo.packageName
									+ ".apk";

						} catch (NameNotFoundException e) {
							e.printStackTrace();
						}
						versionCode = APKFileHandler.getAPKVersionCode(
								mContext, apkFilePath);
						break;
					}

				}
				if (found == false) {
					getOkDialog(ApkFileActivity.this, R.string.dialogWarm,
							R.string.dialogNoInstall, R.string.dialogOK);
				}
			} else {
				getOkDialog(ApkFileActivity.this, R.string.dialogWarm,
						R.string.dialogEnterAppName, R.string.dialogOK);
			}

		}
	};
	private Button.OnClickListener BrowserClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {

			ArrayList<String> extensions = new ArrayList<String>();
			extensions.add(".apk");
			Intent intent1 = new Intent(
					ApkFileActivity.this.getApplicationContext(),
					ApkFileChooserActivity.class);
			intent1.putStringArrayListExtra((String) ApkFileActivity.this
					.getResources().getText(R.string.filter), extensions);
			startActivityForResult(intent1, REQUEST_PATH);
		}
	};

	private Button.OnClickListener VerifyClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {

			case R.id.buttonVerify:
				if (check()) {

					
					
					new VerifyAsyncTask(ApkFileActivity.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					
					putKeywordData();
					pravTask=new  PRAPVerifyAsyncTask(ApkFileActivity.this);
					pravTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					
					
					new AMDRVVerifyAsyncTask(ApkFileActivity.this)
							.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					
					

				}
				break;
			}
		}
	};

	private boolean checkNetWorkConnect() {
		boolean checkCon = true;
		ConnectivityManager mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
		if (mNetworkInfo == null || !mNetworkInfo.isConnected()) {
			checkCon = false;
			wifiManager = (WifiManager) ApkFileActivity.this
					.getSystemService(ApkFileActivity.WIFI_SERVICE);
			if (!wifiManager.isWifiEnabled()) {
				getOkAndCancelDialog(ApkFileActivity.this, R.string.dialogWarm,
						R.string.dialogWifiOpen, R.string.dialogOpen,
						R.string.dialogCancel);
			} else {
				getOkDialog(ApkFileActivity.this, R.string.dialogWarm,
						R.string.dialogChecknetwork, R.string.dialogOK);
			}
		}
		return checkCon;
	}

	protected boolean check() {
		boolean check = false;
		if (textViewApkFileName.getText().toString().equals(""))
			getOkDialog(ApkFileActivity.this, R.string.dialogWarm,
					R.string.selectApkFile, R.string.dialogOK);
		else if (!textViewApkFileName.getText().toString().endsWith(".apk"))
			getOkDialog(ApkFileActivity.this, R.string.dialogWarm,
					R.string.selectApkFile, R.string.dialogOK);
		else if (textViewApkFileName.getText().toString()
				.equals("apkFileName.apk"))
			getOkDialog(ApkFileActivity.this, R.string.dialogWarm,
					R.string.selectApkFile, R.string.dialogOK);
		else
			check = checkNetWorkConnect();
		return check;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == REQUEST_PATH) {
			if (resultCode == RESULT_OK) {
				apkFileName = data.getStringExtra("fileName");
				apkFilePath = data.getStringExtra("filePath");
				versionCode = data.getIntExtra("versionCode", -1);
				textViewApkFileName.setText(apkFileName);
				apkBitmap = ImageTransformer.drawableToBitmap(APKFileHandler
						.getApkIcon(ApkFileActivity.this, apkFilePath));
				imageViewApkIcon.setImageBitmap(apkBitmap);

			}
		}
	}

	public static ProgressDialog showProgressDialog() {
		progressDialog = new ProgressDialog(mContext);
		progressDialog.setMessage(mContext.getResources().getString(
				R.string.conServer));
		progressDialog.setCancelable(false);
		progressDialog.show();
		return progressDialog;
	}

	public void getOkDialog(Context context, int title, int message,
			int dialogok) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setPositiveButton(dialogok,
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(android.content.DialogInterface dialog,
							int whichButton) {
						setResult(RESULT_OK);
					}
				});
		builder.create();
		builder.show();
	}

	private void getOkAndCancelDialog(Context context, int title, int message,
			int dialogok, int dialogcnacel) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setPositiveButton(dialogok,
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(android.content.DialogInterface dialog,
							int i) {
						setResult(RESULT_OK);
						wifiManager.setWifiEnabled(true);
					}
				});
		builder.setNegativeButton(dialogcnacel, null);
		builder.create();
		builder.show();
	}

	private void setView() {
		imageViewApkIcon.setImageBitmap(BitmapFactory.decodeResource(
				getResources(), R.drawable.apk));

		buttonSearch.setImageBitmap(BitmapFactory.decodeResource(
				getResources(), R.drawable.search));

		textViewApkFileName.setText(R.string.textViewapkFileDefault);
		autoCompleteTextViewAppName.setText("");
		
		

	}

	private void putKeywordData() {
		// Intent intent = new Intent();
		// intent.setClass(ApkFileActivity.this, ResultActivity.class);
		//
		// Bundle personalData = new Bundle();
		if (edName.getText().toString().equals("")) {
			attackCriteria[0] = "not value";
		} else {
			attackCriteria[0] = edName.getText().toString();
		}

		if (edPhone.getText().toString().equals("")) {
			attackCriteria[1] = "not value";
		} else {
			attackCriteria[1] = edPhone.getText().toString();
		}

		if (edCity.getText().toString().equals("")) {
			attackCriteria[2] = "not value";
		} else {
			attackCriteria[2] = edCity.getText().toString();
		}

		if (edMail.getText().toString().equals("")) {
			attackCriteria[3] = "not value";
		} else {
			attackCriteria[3] = edMail.getText().toString();
		}

		if (edPassword.getText().toString().equals("")) {
			attackCriteria[4] = "not value";
		} else {
			attackCriteria[4] = edPassword.getText().toString();
		}

		if (edContent.getText().toString().equals("")) {
			attackCriteria[5] = "not value";
		} else {
			attackCriteria[5] = edContent.getText().toString();
		}

		if (edId.getText().toString().equals("")) {
			attackCriteria[6] = "not value";
		} else {
			attackCriteria[6] = edId.getText().toString();
		}

		if (edBday.getText().toString().equals("")) {
			attackCriteria[7] = "not value";
		} else {
			attackCriteria[7] = edBday.getText().toString();
		}

		if (nameC.isChecked()) {
			individualPerception[0] = edName.getText().toString();
			nameWeight = nameW.getProgress();
		} else {
			individualPerception[0] = "not value";
			nameWeight = 0;
		}

		if (phoneC.isChecked()) {
			individualPerception[1] = edPhone.getText().toString();
			phoneWeight = phoneW.getProgress();
		} else {
			individualPerception[1] = "not value";
			phoneWeight = 0;
		}

		if (cityC.isChecked()) {
			individualPerception[2] = edCity.getText().toString();
			cityWeight = cityW.getProgress();
		} else {
			individualPerception[2] = "not value";
			cityWeight = 0;
		}

		if (mailC.isChecked()) {
			individualPerception[3] = edMail.getText().toString();
			mailWeight = mailW.getProgress();
		} else {
			individualPerception[3] = "not value";
			mailWeight = 0;
		}

		if (passwordC.isChecked()) {
			individualPerception[4] = edPassword.getText().toString();
			passwordWeight = passwordW.getProgress();
		} else {
			individualPerception[4] = "not value";
			passwordWeight = 0;
		}
		if (contentC.isChecked()) {
			individualPerception[5] = edContent.getText().toString();
			contentWeight = contentW.getProgress();
		} else {
			individualPerception[5] = "not value";
			contentWeight = 0;
		}
		if (idC.isChecked()) {
			individualPerception[6] = edId.getText().toString();
			idWeight = idW.getProgress();
		} else {
			individualPerception[6] = "not value";
			idWeight = 0;
		}
		if (bdayC.isChecked()) {
			individualPerception[7] = edBday.getText().toString();
			bdayWeight = bdayW.getProgress();
		} else {
			individualPerception[7] = "not value";
			bdayWeight = 0;
		}

		// personalData.putStringArray("attackCriteria", attackCriteria);
		// personalData.putStringArray("individualPerception",
		// individualPerception);
		//
		// intent.putExtras(personalData);
		//
		// startActivity(intent);
	}

	OnSeekBarChangeListener SeekbarClick = new OnSeekBarChangeListener() {

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			// TODO Auto-generated method stub

			if (phoneC.isChecked()) {
				phoneW.setEnabled(true);
			} else {
				phoneW.setEnabled(false);
			}

			if (cityC.isChecked()) {
				cityW.setEnabled(true);
			} else {
				cityW.setEnabled(false);
			}

			if (bdayC.isChecked()) {
				bdayW.setEnabled(true);
			} else {
				bdayW.setEnabled(false);
			}

			if (mailC.isChecked()) {
				mailW.setEnabled(true);
			} else {
				mailW.setEnabled(false);
			}

			if (passwordC.isChecked()) {
				passwordW.setEnabled(true);
			} else {
				passwordW.setEnabled(false);
			}

			if (contentC.isChecked()) {
				contentW.setEnabled(true);
			} else {
				contentW.setEnabled(false);
			}

			if (idC.isChecked()) {
				idW.setEnabled(true);
			} else {
				idW.setEnabled(false);
			}
			nameWvalue.setText(Integer.toString(nameW.getProgress()));
			phoneWvalue.setText(Integer.toString(phoneW.getProgress()));
			cityWvalue.setText(Integer.toString(cityW.getProgress()));
			bdayWvalue.setText(Integer.toString(bdayW.getProgress()));
			mailWvalue.setText(Integer.toString(mailW.getProgress()));
			passwordWvalue.setText(Integer.toString(passwordW.getProgress()));
			contentWvalue.setText(Integer.toString(contentW.getProgress()));
			idWvalue.setText(Integer.toString(idW.getProgress()));

		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub

		}

	};

	OnCheckedChangeListener checkClick = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			// TODO Auto-generated method stub
			if (nameC.isChecked()) {
				nameW.setEnabled(true);
			} else {
				nameW.setEnabled(false);
			}
			if (phoneC.isChecked()) {
				phoneW.setEnabled(true);
			} else {
				phoneW.setEnabled(false);
			}

			if (cityC.isChecked()) {
				cityW.setEnabled(true);
			} else {
				cityW.setEnabled(false);
			}

			if (bdayC.isChecked()) {
				bdayW.setEnabled(true);
			} else {
				bdayW.setEnabled(false);
			}

			if (mailC.isChecked()) {
				mailW.setEnabled(true);
			} else {
				mailW.setEnabled(false);
			}

			if (passwordC.isChecked()) {
				passwordW.setEnabled(true);
			} else {
				passwordW.setEnabled(false);
			}

			if (contentC.isChecked()) {
				contentW.setEnabled(true);
			} else {
				contentW.setEnabled(false);
			}

			if (idC.isChecked()) {
				idW.setEnabled(true);
			} else {
				idW.setEnabled(false);
			}
		}
	};

}
