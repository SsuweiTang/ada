package edu.ntust.cs.most.ada.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import edu.ntust.cs.most.ada.R;


public class DetailResultSub5Activity extends Activity {

	private WebView AARes_detail_Web;
	private String apk_packagename, apk_version, queryIdentifier;
	private String weburl = "http://140.118.110.84:8080/ue-platform/project/showTreeDiagramForMobile/";
	private Button AARes_detail_back;
	private ProgressDialog progressBar;
	private AlertDialog alertDialog;
	private static final String TAG = "Main";
	
	private TextView textViewHeaderTitle;
	private Button buttonHeaderBank;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_view_result);

		Intent intent = getIntent();
		apk_packagename = intent.getStringExtra("apkFileName");
		apk_version = intent.getStringExtra("versionCode");
		queryIdentifier = intent.getStringExtra("Record_QueryIdentifier");
		// weburl = weburl + "apkname=" + apk_packagename + "& apkversion="
		// + apk_version + "& apkusername=" + queryIdentifier;
		weburl = weburl + apk_packagename + apk_version;
		ini_view();
	}

	@SuppressLint("NewApi")
	public void ini_view() {
		
		textViewHeaderTitle = (TextView) findViewById(R.id.textViewHeaderTitle);
		buttonHeaderBank = (Button) findViewById(R.id.buttonHeaderBank);
		buttonHeaderBank.setVisibility(View.VISIBLE);
		textViewHeaderTitle
				.setText(R.string.textViewHeadTitleADAResultDetail_sub5);
		buttonHeaderBank.setOnClickListener(BankClickListener);
		
		
		AARes_detail_Web = (WebView) findViewById(R.id.webView1);
		AARes_detail_Web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		AARes_detail_Web.getSettings().setUseWideViewPort(true);
		AARes_detail_Web.getSettings().setLoadWithOverviewMode(true);
		AARes_detail_Web.getSettings().setSupportZoom(true);
		AARes_detail_Web.getSettings().setJavaScriptEnabled(true);
		AARes_detail_Web.getSettings().setBuiltInZoomControls(true);
		AARes_detail_Web.getSettings().setDisplayZoomControls(false);
		AARes_detail_Web.setWebViewClient(AARes_detail_WebClient);
		AARes_detail_Web.getSettings().setPluginState(PluginState.ON);
		AARes_detail_Web.getSettings().setAllowFileAccess(true);
		AARes_detail_Web.getSettings().setAllowContentAccess(true);

		AARes_detail_Web.loadUrl(weburl);

		android.app.AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		progressBar = ProgressDialog.show(DetailResultSub5Activity.this, "Loading",
				"Loading...");
	}
	
	
	private Button.OnClickListener BankClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			// Intent intent = new Intent();
			// intent.setClass(ResultActivity.this, ApkFileActivity.class);
			// startActivity(intent);
			finish();

		}
	};

	private WebViewClient AARes_detail_WebClient = new WebViewClient() {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			Log.i(TAG, "Finished loading URL: " + url);
			if (progressBar.isShowing()) {
				progressBar.dismiss();
			}
		}

		@SuppressWarnings("deprecation")
		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			Log.e(TAG, "Error: " + description);
			Toast.makeText(DetailResultSub5Activity.this, "Oh no! " + description,
					Toast.LENGTH_SHORT).show();
//			alertDialog.setTitle("Error");
//			alertDialog.setMessage(description);
//			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
//				public void onClick(DialogInterface dialog, int which) {
//					return;
//				}
//			});
//			alertDialog.show();
		}
	};
}
