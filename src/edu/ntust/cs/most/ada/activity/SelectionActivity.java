package edu.ntust.cs.most.ada.activity;

import edu.ntust.cs.most.ada.R;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class SelectionActivity extends Activity {

	private Button buttonFunctionSub3;

	private Button buttonFunctionSub4;

	private Button buttonFunctionSub5;
	private TextView textViewHeaderTitle;
	private Button buttonHeaderBank;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_selection);

		new pageStart().execute();

	}

	private class pageStart extends AsyncTask<Void, Integer, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			buttonFunctionSub3 = (Button) findViewById(R.id.buttonFunctionSub3);
			buttonFunctionSub4 = (Button) findViewById(R.id.buttonFunctionSub4);
			buttonFunctionSub5 = (Button) findViewById(R.id.buttonFunctionSub5);

			setListeners();

			textViewHeaderTitle = (TextView) findViewById(R.id.textViewHeaderTitle);
			buttonHeaderBank = (Button) findViewById(R.id.buttonHeaderBank);
			buttonHeaderBank.setVisibility(View.GONE);
			textViewHeaderTitle.setText(R.string.textViewHeadTitleSelection);

		}

		@Override
		protected String doInBackground(Void... arg0) {

			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}
	}

	private void setListeners() {

		buttonFunctionSub3.setOnClickListener(functionClickListener);
		buttonFunctionSub4.setOnClickListener(functionClickListener);
		buttonFunctionSub5.setOnClickListener(functionClickListener);

	}

	private Button.OnClickListener functionClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();

			switch (v.getId()) {

			case R.id.buttonFunctionSub3:

				intent.putExtra("function", "sub3");

				intent.setClass(SelectionActivity.this,
						ApkFileSub3Activity.class);
				startActivity(intent);

				break;
			case R.id.buttonFunctionSub4:

				intent.putExtra("function", "sub4");

				intent.setClass(SelectionActivity.this,
						ApkFileSub4Activity.class);
				startActivity(intent);

				break;
			case R.id.buttonFunctionSub5:

				intent.putExtra("function", "sub5");

				intent.setClass(SelectionActivity.this,
						ApkFileSub5Activity.class);
				startActivity(intent);

				break;

			}

		}
	};

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

			AlertDialog.Builder dialog = new AlertDialog.Builder(
					SelectionActivity.this);
//			dialog.setTitle(R.string.dialogExit);
			dialog.setMessage(R.string.dialogExit);
			dialog.setPositiveButton(R.string.dialogOK,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialoginterface,
								int i) {
							finish();

						}
					});
			dialog.setNegativeButton(R.string.dialogCancel, null);
			dialog.show();
			return true;

		}
		return super.onKeyDown(keyCode, event);
	}

}
