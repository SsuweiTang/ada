package edu.ntust.cs.most.ada.activity;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.ntust.cs.most.ada.R;
import edu.ntust.cs.most.ada.adapter.FileChooserArrayAdapter;
import edu.ntust.cs.most.ada.domain.FileChooserItem;
import edu.ntust.cs.most.ada.util.APKFileHandler;
import edu.ntust.cs.most.ada.util.FileHandler;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * 
 * @author Ssu-Wei,Tang
 *
 */

public class ApkFileChooserInstallActivity extends Activity {

	private ListView listViewFileChooser;
	private File currentDir;
	private FileChooserArrayAdapter adapterFileChooserArray;
	private File fileSelected;
	private FileFilter fileFilter;
	private ArrayList<String> extensions;
	private TextView textViewPath;
	private TextView textViewHeadTitle;
	
	private String apkFilePath;
	private String apkFileName;
	
	private Button buttonHeaderBank;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_apkfile_chooser);
		findView();
		
		fill();
	}

	

	private void fill() {
		
//		File[] dirs = null;
//		List<FileChooserItem> dir = new ArrayList<FileChooserItem>();
//		List<FileChooserItem> fls = new ArrayList<FileChooserItem>();
//		if (fileFilter != null)
//			dirs = currentDir.listFiles(fileFilter);
//		else 
//			dirs = currentDir.listFiles();
//		textViewPath.setText(getString(R.string.currentDir) + ": "	+ currentDir.getName());
//			
////		this.setTitle(getString(R.string.currentDir) + ": "	+ currentDir.getName());
//		try {
//			for (File ff : dirs) {
//				if (ff.isDirectory() && !ff.isHidden()) {
//					dir.add(new FileChooserItem(ff.getName(),getString(R.string.folder)	+ ":"+ String.valueOf(FileHandler.getFilelistSize(ff,fileFilter)), FileHandler.getFileLastDate(ff), ff.getAbsolutePath(),true, false));
//				} else {
//					if (!ff.isHidden()) {
//						fls.add(new FileChooserItem(ff.getName(),getString(R.string.fileSize) + ": "+ ff.length(), FileHandler.getFileLastDate(ff), ff.getAbsolutePath(), false, false));
//					}
//				}
//			}
//		} catch (Exception e) {
//		}
//		Collections.sort(dir);
//		Collections.sort(fls);
//		dir.addAll(fls);
//
//		if (!currentDir.getName().equalsIgnoreCase("sdcard")) {
//			if (currentDir.getParentFile() != null)
//				dir.add(0,new FileChooserItem("..",getString(R.string.parentDirectory), "",currentDir.getParent(), false, true));
//		}
//		adapterFileChooserArray = new FileChooserArrayAdapter(ApkFileChooserInstallActivity.this,R.layout.list_item_apkfile_chooser, dir);
//		listViewFileChooser.setAdapter(adapterFileChooserArray);
		
		
		
		
		List<FileChooserItem> fls = new ArrayList<FileChooserItem>();
		List<PackageInfo> appList = getAllApps(ApkFileChooserInstallActivity.this);
		PackageInfo packageinfo;
		PackageManager packageManager = ApkFileChooserInstallActivity.this
				.getPackageManager();
		
	
		
		for (int i = 0; i < appList.size(); i++) {
			packageinfo = appList.get(i);
			
			try {
				ApplicationInfo AI = packageManager
						.getApplicationInfo(
								packageinfo.packageName,
								PackageManager.GET_META_DATA
										| PackageManager.GET_SHARED_LIBRARY_FILES);
				apkFilePath = AI.sourceDir;
				apkFileName = packageinfo.applicationInfo.packageName
						+ ".apk";

			} catch (NameNotFoundException e) {
				e.printStackTrace();
			}
			
			fls.add(new FileChooserItem(packageManager.getApplicationLabel(packageinfo.applicationInfo).toString(),getString(R.string.fileSize) + ": "+ "", apkFileName, apkFilePath, false, false));
			
			

		}
		
		adapterFileChooserArray = new FileChooserArrayAdapter(ApkFileChooserInstallActivity.this,R.layout.list_item_apkfile_chooser, fls);
		listViewFileChooser.setAdapter(adapterFileChooserArray);

	}
	
	
	public static List<PackageInfo> getAllApps(Context coNtext) {
		List<PackageInfo> apps = new ArrayList<PackageInfo>();
		PackageManager packageManager = coNtext.getPackageManager();
		List<PackageInfo> paklist = packageManager.getInstalledPackages(0);
		for (int i = 0; i < paklist.size(); i++) {
			PackageInfo pak = (PackageInfo) paklist.get(i);
			if ((pak.applicationInfo.flags & pak.applicationInfo.FLAG_SYSTEM) <= 0) {
				apps.add(pak);
			}
		}
		return apps;
	}

	private void findView() {
		listViewFileChooser = (ListView) findViewById(R.id.listViewFileChooser);
		textViewPath=(TextView)findViewById(R.id.textViewPath);
		textViewPath.setVisibility(View.GONE);
		listViewFileChooser	.setOnItemClickListener(fileChooserItemClickListener);
		textViewHeadTitle=(TextView)findViewById(R.id.textViewHeaderTitle);
		textViewHeadTitle.setText(R.string.textViewHeadTitleMyFileInstall);
		
		buttonHeaderBank = (Button) findViewById(R.id.buttonHeaderBank);
		buttonHeaderBank.setVisibility(View.VISIBLE);
		buttonHeaderBank.setOnClickListener(BankClickListener);
		

	}
	private Button.OnClickListener BankClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			// Intent intent = new Intent();
			// intent.setClass(ResultActivity.this, ApkFileActivity.class);
			// startActivity(intent);
			finish();

		}
	};

	private OnItemClickListener fileChooserItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> a, View v, int position, long id) {
			FileChooserItem fileChooserItem = adapterFileChooserArray
					.getItem(position);
		System.out.println("name"+fileChooserItem.getDate());
		

				if (fileChooserItem.getDate().endsWith(".apk")) {
					Toast.makeText(ApkFileChooserInstallActivity.this,	"File Clicked: " + fileChooserItem.getPath(),Toast.LENGTH_SHORT).show();
					Intent intent = new Intent();
					intent.putExtra("fileName", fileChooserItem.getDate());
					intent.putExtra("filePath", fileChooserItem.getPath());
					intent.putExtra("versionCode", APKFileHandler.getAPKVersionCode(ApkFileChooserInstallActivity.this,fileChooserItem.getPath()));
					setResult(Activity.RESULT_OK, intent);
					finish();

				

			}

		}
	};

}
