package edu.ntust.cs.most.ada.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import edu.ntust.cs.most.ada.R;
import edu.ntust.cs.most.ada.task.PRAPVerifyAsyncTask;

public class ResultActivity extends Activity {

	public TextView textViewVerifyResult;
	public TextView textViewVerifyResult3;
	public TextView textViewToastResult3;
	public TextView textVerifyResultDetail;
	public ImageView imageViewResult;
	
	private Button detailclick;
	public TextView textVerifyResultDetailTitle;
	private Context mContext;
	private String resultCode;
	private String resultDetail;
	private String apkFilePath;
	private String apkFileName;
	private String versionCode;
	private TextView textViewHeaderTitle;
	private Button buttonHeaderBank;

	public TextView AMDRVtextAPKName;
	public TextView AMDRVtextStartTime;
	public TextView AMDRVtextEndTime;
	public TextView AMDRVtextDuringTime;
	public TextView AMDRVtextStatus;
	private Button AMDRVbuttonResult;
	
	private String status = "";
	private String dbcheck = "";
	private Bitmap apkBitmap;
	private int apkposition;
	private int showVar = 0;
	private double privacyRisk = 0;
	SpannableString AnalysisResult = null;
	SpannableString toastStSpannableString = null;
	//private Button buttonAMDRVResult;
	
	private LinearLayout liRisk;
	
	
	
	ArrayList<String> tableNameList = new ArrayList<String>();
	ArrayList<String> columnNameList = new ArrayList<String>();
	ArrayList<String> acPrivcyData = new ArrayList<String>();
	ArrayList<String> ac1PrivcyData = new ArrayList<String>();
	ArrayList<String> ac2PrivcyData = new ArrayList<String>();
	ArrayList<String> ipPrivcyData = new ArrayList<String>();
	ArrayList<String> ip1PrivcyData = new ArrayList<String>();
	ArrayList<String> ip2PrivcyData = new ArrayList<String>();
	ArrayList<String> databaseName = new ArrayList<String>(Arrays.asList("",
			"", "", ""));
	ArrayList<Double> PLbetaList = new ArrayList<Double>();
	ArrayList<Double> SortPLbetaList = new ArrayList<Double>();
	ArrayList<Integer> filePlNonZero = new ArrayList<Integer>();
	
	
	
	private LinearLayout include_sub4;
	private LinearLayout include_sub5;
	
	private int screenWidth;
	private int screenHeight;
	private int dialgoWidth;
	private int dialgoheight;


	private View sub4PopupWindow_view;
	private PopupWindow sub4PopupWindow;
	
	private View sub5PopupWindow_view;
	private PopupWindow sub5PopupWindow;
	
	private Button buttonVerifyResultDetail;
	
	
	private  TextView popSub4TextVerifyResultDetail;
	private  TextView popSub4TextVerifyResultDetailTitle;
	private  ImageButton popSub4ButtonDelete ;
	private  Button popSub4ButtonClose ;
	private  Button popSub5ButtonClose ;
	private  TextView textViewApkFileName;
	private ImageView imageViewApkIcon;
	
	
	private WebView popSub5AARes_detail_Web;
	private String apk_packagename, apk_version, queryIdentifier;
	private String weburl = "http://140.118.110.84:8080/ue-platform/project/showTreeDiagramForMobile/";
	private Button AARes_detail_back;
	private ProgressDialog progressBar;
	private AlertDialog alertDialog;
	private static final String TAG = "Main";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_result);
		mContext = this;
		findView();
		setListeners();

	}

	private void setListeners() {
		
		
	
		buttonHeaderBank.setOnClickListener(BankClickListener);
		AMDRVbuttonResult.setOnClickListener(AMDRVClickListener);
	
		
		include_sub4.setOnClickListener(Sub4ClickListener);
		include_sub5.setOnClickListener(Sub5ClickListener);
		
		buttonVerifyResultDetail.setOnClickListener(Sub4ResultDetailClickListener);
		
	}

	private void findView() {

		textViewVerifyResult = (TextView) findViewById(R.id.textVerifyResult);
		imageViewResult = (ImageView) findViewById(R.id.imageViewResult);
	
		
		textViewHeaderTitle = (TextView) findViewById(R.id.textViewHeaderTitle);
		buttonHeaderBank=(Button)findViewById(R.id.buttonHeaderBank);
		buttonHeaderBank.setVisibility(View.VISIBLE);
	
		textViewHeaderTitle.setText(R.string.textViewHeadTitleADAResult);
		buttonVerifyResultDetail=(Button)findViewById(R.id.buttonVerifyResultDetail);
		
		detailclick = (Button) findViewById(R.id.detailclick);
		detailclick.setOnClickListener(PRAPDetailClickListener);
		
		
		liRisk=(LinearLayout)findViewById(R.id.liRisk);
		
		
		
		textViewApkFileName = (TextView) findViewById(R.id.textViewApkFileName);
		imageViewApkIcon = (ImageView) findViewById(R.id.imageViewApkIcon);
		
		textViewVerifyResult3 = (TextView) findViewById(R.id.textResult3);
		textViewToastResult3 = (TextView) findViewById(R.id.ToastResult3);
		
		
		
		AMDRVtextAPKName= (TextView) findViewById(R.id.textAPKName);
    	AMDRVtextStartTime= (TextView) findViewById(R.id.textStartTime);
    	AMDRVtextEndTime= (TextView) findViewById(R.id.textEndTime);
    	AMDRVtextDuringTime= (TextView) findViewById(R.id.textDuringTime);
    	AMDRVtextStatus= (TextView) findViewById(R.id.textStatus);
		AMDRVbuttonResult = (Button) findViewById(R.id.buttonResult);
		
		
		include_sub4=(LinearLayout)findViewById(R.id.include_sub4);
		include_sub5=(LinearLayout)findViewById(R.id.include_sub5);
		intiView();
		//getPRAPResult();
		getResult();
		getAMDRVResult();
	
	}

	

	private void getResult() {

		Bundle resultBundle = ResultActivity.this.getIntent().getExtras();
		resultCode = resultBundle.getString("resultCode");
		apkFileName = resultBundle.getString("apkFileName");
		resultDetail = resultBundle.getString("resultDetail");
		apkFilePath = resultBundle.getString("apkFilePath");
		apkBitmap = (Bitmap) ResultActivity.this.getIntent().getParcelableExtra("apkBitmap");
		imageViewApkIcon.setImageBitmap(apkBitmap);
		
	
		textViewApkFileName.setText(apkFileName);
		
		buttonVerifyResultDetail.setVisibility(View.GONE);
		if (resultCode.toString().equals("1")) {

			imageViewResult.setImageResource(R.drawable.result_green);
			textViewVerifyResult.setText(R.string.sub4_verifyResult1);
		} else if (resultCode.toString().equals("3")) {
			imageViewResult.setImageResource(R.drawable.result_yellow);
			textViewVerifyResult.setText(R.string.sub4_verifyResult3);

		} else if (resultCode.toString().equals("2")) {
			imageViewResult.setImageResource(R.drawable.result_blue);
			textViewVerifyResult.setText(R.string.sub4_verifyResult2);

		} else if (resultCode.toString().equals("4")) {
			buttonVerifyResultDetail.setVisibility(View.VISIBLE);

			imageViewResult.setImageResource(R.drawable.result_red);
			textViewVerifyResult.setText(R.string.sub4_verifyResult4);
			
		} else if (resultCode.toString().equals("5")) {
			textViewVerifyResult.setText("Apk file upload failed.");
			imageViewResult.setImageResource(R.drawable.result_orange);

		} else if (resultCode.toString().equals("0")) {
			imageViewResult.setImageResource(R.drawable.result_orange);
			textViewVerifyResult.setText(R.string.sub4_verifyResult0);

		}
		
		/**sub3**/
		status = resultBundle.getString("status");
		dbcheck = resultBundle.getString("dbcheck");
		if(status.equals("uninstall")){
			textViewVerifyResult3.setText(R.string.sub3_verifyResult_uninstall);
			textViewToastResult3.setVisibility(View.GONE);
			detailclick.setClickable(false);
			liRisk.setVisibility(View.GONE);
			
			
			detailclick.setTextColor(Color.GRAY);
			detailclick.setVisibility(View.INVISIBLE);
		}else{
			if(!dbcheck.equals("true")){
				textViewVerifyResult3.setText(R.string.sub3_verifyResult_noDB);
				textViewToastResult3.setVisibility(View.GONE);
				detailclick.setClickable(false);
				detailclick.setTextColor(Color.GRAY);
				detailclick.setVisibility(View.INVISIBLE);
				liRisk.setVisibility(View.GONE);
				
			}else{
				tableNameList = resultBundle.getStringArrayList("tableNameList");
				columnNameList = resultBundle.getStringArrayList("columnNameList");
				acPrivcyData = resultBundle.getStringArrayList("acPrivcyData");
				ac1PrivcyData = resultBundle.getStringArrayList("ac1PrivcyData");
				ac2PrivcyData = resultBundle.getStringArrayList("ac2PrivcyData");
				ipPrivcyData = resultBundle.getStringArrayList("ipPrivcyData");
				ip1PrivcyData = resultBundle.getStringArrayList("ip1PrivcyData");
				ip2PrivcyData = resultBundle.getStringArrayList("ip2PrivcyData");
				ipPrivcyData = resultBundle.getStringArrayList("ipPrivcyData");		
				databaseName = resultBundle.getStringArrayList("databaseName");
				columnNameList = resultBundle.getStringArrayList("columnNameList");
				
				AnalysisResult = new SpannableString(resultBundle.getString("privacyRiskShow"));
				toastStSpannableString = new SpannableString(resultBundle.getString("toastRisk"));
				apkBitmap = (Bitmap) ResultActivity.this.getIntent().getParcelableExtra("apkBitmap");
				PLbetaList = (ArrayList<Double>) ResultActivity.this.getIntent().getSerializableExtra("PLbetaList");
				SortPLbetaList = (ArrayList<Double>) ResultActivity.this.getIntent().getSerializableExtra("SortPLbetaList");
				filePlNonZero = (ArrayList<Integer>) ResultActivity.this.getIntent().getSerializableExtra("filePlNonZero");
				apkposition = resultBundle.getInt("apkposition");
				showVar = resultBundle.getInt("showVar");
				privacyRisk = resultBundle.getDouble("privacyRisk");
				
				if (privacyRisk > 0.5) {
					AnalysisResult.setSpan(
							new ForegroundColorSpan(
									Color.rgb(255, 0, 0)),
									PRAPVerifyAsyncTask.risklen,
									PRAPVerifyAsyncTask.risklen2,
							Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					toastStSpannableString
							.setSpan(
									new RelativeSizeSpan(
											1.3f),
									0,
									PRAPVerifyAsyncTask.toastlen,
									Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					toastStSpannableString.setSpan(
							new ForegroundColorSpan(
									Color.rgb(255, 0, 0)),
							0,
							PRAPVerifyAsyncTask.toastlen,
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					
				} else if (0.5 > privacyRisk
						&& privacyRisk > 0.25) {
					AnalysisResult
							.setSpan(
									new ForegroundColorSpan(
											Color.rgb(
													255,
													110,
													0)),
													PRAPVerifyAsyncTask.risklen,
													PRAPVerifyAsyncTask.risklen2,
									Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					toastStSpannableString
							.setSpan(
									new RelativeSizeSpan(
											1.3f),
									0,
									PRAPVerifyAsyncTask.toastlen,
									Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					toastStSpannableString
							.setSpan(
									new ForegroundColorSpan(
											Color.rgb(
													255,
													110,
													0)),
									0,
									PRAPVerifyAsyncTask.toastlen,
									Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

				} else if (0.25 > privacyRisk) {
					AnalysisResult
							.setSpan(
									new ForegroundColorSpan(
											Color.rgb(
													0,
													128,
													64)),
													PRAPVerifyAsyncTask.risklen,
													PRAPVerifyAsyncTask.risklen2,
									Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					toastStSpannableString
							.setSpan(
									new RelativeSizeSpan(
											1.3f),
									0,
									PRAPVerifyAsyncTask.toastlen,
									Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					toastStSpannableString
							.setSpan(
									new ForegroundColorSpan(
											Color.rgb(
													0,
													128,
													64)),
									0,
									PRAPVerifyAsyncTask.toastlen,
									Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
				textViewVerifyResult3.setText(AnalysisResult);
				textViewToastResult3.setText(toastStSpannableString);
			}
		}
		
	}
	
	private void getAMDRVResult() {

		Bundle resultBundle = ResultActivity.this.getIntent().getExtras();
		resultCode = resultBundle.getString("resultCode");
		apkFileName = resultBundle.getString("apkFileName");
		resultDetail = resultBundle.getString("resultDetail");
		apkFilePath = resultBundle.getString("apkFilePath");
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd  HH:mm");
		Date current = new Date();

		if(!resultCode.toString().equals("0")){
			AMDRVtextAPKName.setText(apkFileName);
	    	AMDRVtextStartTime.setText(sdFormat.format(current));
	    	AMDRVtextEndTime.setText("");
	    	AMDRVtextDuringTime.setText("");
	    	AMDRVtextStatus.setText("");
	    	//new uploadapk("").execute();
		}else{
			AMDRVtextAPKName.setText(apkFileName);
	    	AMDRVtextStartTime.setText(sdFormat.format(current));
	    	AMDRVtextEndTime.setText("");
	    	AMDRVtextDuringTime.setText("");
	    	AMDRVtextStatus.setText("");
		}
	}

	protected void intiView() {
		imageViewResult.setImageResource(0);
		textViewVerifyResult.setText("");
		
	}

	private Button.OnClickListener DeleteClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
				intent.setData(Uri.parse("package:"
						+ apkFileName.replace(".apk", "")));
				startActivity(intent);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	};
	
	private View.OnClickListener Sub4ResultDetailClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			intent.setClass(mContext, DetailResultSub4Activity.class);
		
			Bundle bundle = new Bundle();
			bundle.putString("resultCode", resultCode);
			bundle.putString("resultDetail", resultDetail);
			bundle.putString("apkFilePath", ApkFileActivity.apkFilePath);
			bundle.putString("apkFileName", ApkFileActivity.apkFileName);
			intent.putExtra("apkBitmap", ApkFileActivity.apkBitmap);
			
			intent.putExtras(bundle);
			mContext.startActivity(intent);
//			((Activity)mContext).finish();

	
		}

	};
	
	
	private View.OnClickListener Sub4ClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {

			initPopupWindow(R.layout.pop_resultdetail_sub4);

			if (resultCode.toString().equals("4")) {

				sub4PopupWindow.showAtLocation(v, Gravity.BOTTOM, 15, 90);

				// System.out.println(700+" h  "+screenHeight/2);
//				 popupWindow.showAsDropDown(v, 700, 900);

//				popSub4TextViewVerifyResult.setText(R.string.sub4_verifyResult4);
				popSub4TextVerifyResultDetail.setText(resultDetail);
				popSub4TextVerifyResultDetailTitle.setText(R.string.sub4_verifyResult4_title);
				if (apkFilePath.contains("/data/app"))
					popSub4ButtonDelete.setVisibility(View.VISIBLE);
				else
					popSub4ButtonDelete.setVisibility(View.GONE);

			}
		}

	};
	
	
	private View.OnClickListener Sub5ClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {

			initPopupWindow(R.layout.pop_resultdetail_sub5);

		

//				sub5PopupWindow.showAtLocation(v, Gravity.TOP, 0, 0);
				
//				Bundle resultBundle = ResultActivity.this.getIntent().getExtras();
//				apkFileName = resultBundle.getString("apkFileName");
//				apkFilePath = resultBundle.getString("apkFilePath");
//				versionCode = resultBundle.getString("versionCode");
//				
//				apk_packagename =apkFileName;
//				apk_version = versionCode;
//
//				weburl = weburl + apk_packagename + apk_version;
//				
//				
//				popSub5AARes_detail_Web.loadUrl(weburl);
//
//				android.app.AlertDialog.Builder alertDialog = new AlertDialog.Builder(ResultActivity.this);
//				progressBar = ProgressDialog.show(ResultActivity.this, "Loading",
//						"Loading...");
			

			
		}

	};
	
	
	@SuppressWarnings("deprecation")
	private void initPopupWindow(int v) {
		
		
		switch (v) {
		case R.layout.pop_resultdetail_sub4:
			
			sub4PopupWindow_view = getLayoutInflater().inflate(R.layout.pop_resultdetail_sub4, null, false);
			screenWidth = ResultActivity.this.getWindowManager().getDefaultDisplay().getWidth();
			screenHeight = ResultActivity.this.getWindowManager().getDefaultDisplay().getHeight();
			dialgoWidth = screenWidth / 6 * 5;
			dialgoheight = screenHeight / 3;

			sub4PopupWindow = new PopupWindow(sub4PopupWindow_view, dialgoWidth,dialgoheight, true);
			
			
			popSub4TextVerifyResultDetail = (TextView) sub4PopupWindow_view.findViewById(R.id.textVerifyResultDetail);
			popSub4TextVerifyResultDetailTitle = (TextView) sub4PopupWindow_view.findViewById(R.id.textVerifyResultDetailTitle);
			popSub4ButtonDelete = (ImageButton) sub4PopupWindow_view.findViewById(R.id.buttonDelete);
			popSub4ButtonClose = (Button) sub4PopupWindow_view.findViewById(R.id.ButtonClose);
			popSub4ButtonDelete.setOnClickListener(DeleteClickListener);
			popSub4ButtonClose.setOnClickListener(sub4CloseClickListener);
			
			
			break;
			
			
		case R.layout.pop_resultdetail_sub5:
			
			
			sub5PopupWindow_view = getLayoutInflater().inflate(R.layout.pop_resultdetail_sub5, null, false);
			screenWidth = ResultActivity.this.getWindowManager().getDefaultDisplay().getWidth();
			screenHeight = ResultActivity.this.getWindowManager().getDefaultDisplay().getHeight();
			dialgoWidth = screenWidth/10*9;
			dialgoheight = screenHeight/10*9;

			sub5PopupWindow = new PopupWindow(sub5PopupWindow_view, dialgoWidth,dialgoheight, true);
			
			popSub5AARes_detail_Web= (WebView) sub5PopupWindow_view.findViewById(R.id.webView1);
			popSub5AARes_detail_Web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
			popSub5AARes_detail_Web.getSettings().setSupportZoom(true);
			popSub5AARes_detail_Web.getSettings().setJavaScriptEnabled(true);
			popSub5AARes_detail_Web.getSettings().setBuiltInZoomControls(true);
			popSub5AARes_detail_Web.getSettings().setDisplayZoomControls(false);
			popSub5AARes_detail_Web.setWebViewClient(AARes_detail_WebClient);
			popSub5AARes_detail_Web.getSettings().setPluginState(PluginState.ON);
			popSub5AARes_detail_Web.getSettings().setAllowFileAccess(true);
			popSub5AARes_detail_Web.getSettings().setAllowContentAccess(true);
			
			
			popSub5ButtonClose = (Button) sub5PopupWindow_view.findViewById(R.id.ButtonClose);
			popSub5ButtonClose.setOnClickListener(sub5CloseClickListener);
			
		
			
			break;
		
		}
		
		
	}
	
	private WebViewClient AARes_detail_WebClient = new WebViewClient() {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			Log.i(TAG, "Finished loading URL: " + url);
			if (progressBar.isShowing()) {
				progressBar.dismiss();
			}
		}

		@SuppressWarnings("deprecation")
		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			Log.e(TAG, "Error: " + description);
			Toast.makeText(ResultActivity.this, "Oh no! " + description,
					Toast.LENGTH_SHORT).show();
			alertDialog.setTitle("Error");
			alertDialog.setMessage(description);
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					return;
				}
			});
			alertDialog.show();
		}
	};
	
	
	private Button.OnClickListener sub4CloseClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
				
				if(sub4PopupWindow!=null)
					sub4PopupWindow.dismiss();
		}
	};
	
	private Button.OnClickListener sub5CloseClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
				
				if(sub5PopupWindow!=null)
					sub5PopupWindow.dismiss();
		}
	};
	
	private Button.OnClickListener BankClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
//			Intent intent = new Intent();
//			intent.setClass(ResultActivity.this, ApkFileActivity.class);
//			startActivity(intent);
			finish();

		}
	};
	
	private Button.OnClickListener AMDRVClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			Bundle resultBundle = ResultActivity.this.getIntent().getExtras();
			apkFileName = resultBundle.getString("apkFileName");
			apkFilePath = resultBundle.getString("apkFilePath");
			versionCode = resultBundle.getString("versionCode");
			
			Intent intent = new Intent();
			intent.putExtra("Record_ApkpackageName", apkFileName);
			intent.putExtra("Record_Apkversion", versionCode);
			intent.putExtra("Record_QueryIdentifier", "");
			intent.setClass(ResultActivity.this, DetailResultSub5Activity.class);
			startActivity(intent);
		}
	};
	
	private Button.OnClickListener PRAPDetailClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			intent.setClass(mContext, DetailResultSub3Activity.class);
			Bundle bundle = new Bundle();			
			bundle.putString("apkFileName", apkFileName);
			bundle.putInt("apkposition", apkposition);
			bundle.putInt("showVar", showVar);
			bundle.putDouble("privacyRisk", privacyRisk);
			bundle.putStringArrayList("tableNameList", tableNameList);
			bundle.putStringArrayList("columnNameList", columnNameList);
			bundle.putStringArrayList("acPrivcyData", acPrivcyData);
			bundle.putStringArrayList("ac1PrivcyData", ac1PrivcyData);
			bundle.putStringArrayList("ac2PrivcyData", ac2PrivcyData);
			bundle.putStringArrayList("ipPrivcyData", ipPrivcyData);
			bundle.putStringArrayList("ip1PrivcyData", ip1PrivcyData);
			bundle.putStringArrayList("ip2PrivcyData", ip2PrivcyData);
			bundle.putStringArrayList("databaseName", databaseName);
			intent.putExtras(bundle);
			intent.putExtra("apkBitmap", apkBitmap);
			intent.putExtra("PLbetaList", PLbetaList);
			intent.putExtra("SortPLbetaList", SortPLbetaList);
			intent.putExtra("filePlNonZero", filePlNonZero);
			startActivity(intent);
		}
	};

}
