package edu.ntust.cs.most.ada.activity;

import edu.ntust.cs.most.ada.R;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailResultSub4Activity extends Activity {

	private TextView textVerifyResultDetail;
	private TextView textVerifyResultDetailTitle;
	private ImageButton buttonDelete;
	private String resultCode;
	private String apkFileName;
	private String resultDetail;
	private String apkFilePath;
	
	private TextView textViewHeaderTitle;
	private Button buttonHeaderBank;
	private TextView textVerifyResultNotInstall;
	private  TextView textViewApkFileName;
	private ImageView imageViewApkIcon;
	
	private Bitmap apkBitmap;
	
	
	private TextView textVerifyResult;
	private ImageView imageViewResult;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_resultdetail_sub4);
		new pageStart().execute();

	}

	private class pageStart extends AsyncTask<Void, Integer, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			textVerifyResultDetail = (TextView) findViewById(R.id.textVerifyResultDetail);
			textVerifyResultDetailTitle = (TextView) findViewById(R.id.textVerifyResultDetailTitle);
			
			buttonDelete = (ImageButton) findViewById(R.id.buttonDelete);
			buttonDelete.setOnClickListener(DeleteClickListener);
			
			textViewApkFileName = (TextView) findViewById(R.id.textViewApkFileName);
			imageViewApkIcon = (ImageView) findViewById(R.id.imageViewApkIcon);
			
			textVerifyResult=(TextView) findViewById(R.id.textVerifyResult);
			
			imageViewResult = (ImageView) findViewById(R.id.imageViewResult);
			textVerifyResultNotInstall = (TextView) findViewById(R.id.textVerifyResultNotInstall);
		
			textViewHeaderTitle = (TextView) findViewById(R.id.textViewHeaderTitle);
			buttonHeaderBank = (Button) findViewById(R.id.buttonHeaderBank);
			buttonHeaderBank.setVisibility(View.VISIBLE);
			textViewHeaderTitle
					.setText(R.string.textViewHeadTitleADAResultDetail_sub4);
			buttonHeaderBank.setOnClickListener(BankClickListener);
			
			
			textVerifyResultNotInstall.setVisibility(View.GONE);
			buttonDelete.setVisibility(View.GONE);
			

		}

		@Override
		protected String doInBackground(Void... arg0) {

			Bundle resultBundle = DetailResultSub4Activity.this.getIntent()
					.getExtras();
			resultCode = resultBundle.getString("resultCode");
			apkFileName = resultBundle.getString("apkFileName");
			resultDetail = resultBundle.getString("resultDetail");
			apkFilePath = resultBundle.getString("apkFilePath");
			apkBitmap = (Bitmap) DetailResultSub4Activity.this.getIntent().getParcelableExtra("apkBitmap");
			

			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			imageViewApkIcon.setImageBitmap(apkBitmap);
		
			textViewApkFileName.setText(apkFileName);
			
			
			
			if (resultCode.toString().equals("1")) {

				imageViewResult.setImageResource(R.drawable.result_green);
				textVerifyResult.setText(R.string.sub4_verifyResult1);
			} else if (resultCode.toString().equals("3")) {
				imageViewResult.setImageResource(R.drawable.result_yellow);
				textVerifyResult.setText(R.string.sub4_verifyResult3);

			} else if (resultCode.toString().equals("2")) {
				imageViewResult.setImageResource(R.drawable.result_blue);
				textVerifyResult.setText(R.string.sub4_verifyResult2);

			} else if (resultCode.toString().equals("4")) {
				

				imageViewResult.setImageResource(R.drawable.result_red);
				textVerifyResult.setText(R.string.sub4_verifyResult4);
				
				textVerifyResultDetail.setText(resultDetail);
				textVerifyResultDetailTitle
						.setText(R.string.sub4_verifyResult4_title);
				if (apkFilePath.contains("/data/app")) {
					buttonDelete.setVisibility(View.VISIBLE);
					textVerifyResultNotInstall
							.setText(R.string.sub4_verifyResult_delete);
					textVerifyResultNotInstall.setVisibility(View.VISIBLE);
				} else {
					buttonDelete.setVisibility(View.GONE);
					textVerifyResultNotInstall
							.setText(R.string.sub4_verifyResult_notInstall);
				}
				
			} else if (resultCode.toString().equals("5")) {
				textVerifyResult.setText("Apk file upload failed.");
				imageViewResult.setImageResource(R.drawable.result_orange);

			} else if (resultCode.toString().equals("0")) {
				imageViewResult.setImageResource(R.drawable.result_orange);
				textVerifyResult.setText(R.string.sub4_verifyResult0);

			}
			
			

			

		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}
	}

	private Button.OnClickListener DeleteClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
				 intent.setData(Uri.parse("package:"
				 + apkFileName.replace(".apk", "")));
				startActivity(intent);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	};

	private Button.OnClickListener BankClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			// Intent intent = new Intent();
			// intent.setClass(ResultActivity.this, ApkFileActivity.class);
			// startActivity(intent);
			finish();

		}
	};

}
