package edu.ntust.cs.most.ada.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;

public class AppInfo {
	private String appLabel;    //應用程序標籤
    private Drawable appIcon ;  //應用程序圖示
    private Intent intent ;     //啟動應用程序的Intent，一般是Action為Main和Category為Launcher的Activity
    private String pkgName ;    //应用程序所对应的包名
      
    public AppInfo(){}
      
    public String getAppLabel() {
        return appLabel;
    }
    public void setAppLabel(String appName) {
        this.appLabel = appName;
    }
    public Drawable getAppIcon() {
        return appIcon;
    }
    public void setAppIcon(Drawable appIcon) {
        this.appIcon = appIcon;
    }
    public Intent getIntent() {
        return intent;
    }
    public void setIntent(Intent intent) {
        this.intent = intent;
    }
    public String getPkgName(){
        return pkgName;
    }
    public void setPkgName(String pkgName){
        this.pkgName=pkgName;
    }

}
