package edu.ntust.cs.most.ada.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import edu.ntust.cs.most.ada.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class DetailResultSub3Activity extends Activity {
	
	Cursor C;
	
	final File pasteFilePath = new File("/sdcard/Download");
	private int apkposition;
	private Bitmap apkBitmap;
	private String apkFileName;
	private String[] Filename = null;
	public ProgressDialog PDialog = null;
	
	ListView appListView;
	TextView textViewApkFileName;
	ImageView imageViewApkIcon;
	ImageView imageViewFolder;
	Button showResult;
	
	private String[] attackCriteria = new String[8];
	private String[] individualPerception = new String[8];
	
	ArrayList<String> tableNameList = new ArrayList<String>();
	ArrayList<String> columnNameList = new ArrayList<String>();
	ArrayList<String> acPrivcyData = new ArrayList<String>();
	ArrayList<String> ac1PrivcyData = new ArrayList<String>();
	ArrayList<String> ac2PrivcyData = new ArrayList<String>();
	ArrayList<String> ipPrivcyData = new ArrayList<String>();
	ArrayList<String> ip1PrivcyData = new ArrayList<String>();
	ArrayList<String> ip2PrivcyData = new ArrayList<String>();
	ArrayList<String> databaseName = new ArrayList<String>(Arrays.asList("",
			"", "", ""));
	ArrayList<Double> PLbetaList = new ArrayList<Double>();
	ArrayList<Double> SortPLbetaList = new ArrayList<Double>();
	ArrayList<Integer> filePlNonZero = new ArrayList<Integer>();
	
	double acPr1, acRe1, ipPr1, ipRe1, acPr2, acRe2, ipPr2, ipRe2, ac1Fmeasure,
	ac2Fmeasure, ip1Fmeasure, ip2Fmeasure, PLbeta1, PLbeta2;
	
	double privacyRisk = 0.0;
	double alpha = 0.5, beta = 0.5;
	int showVar = 0;
	int clickNumber = 0;
	String analysisDbFilePath = null;
	SpannableString AnalysisResult = null;
	SpannableString titleString = null;
	
	private TextView textViewHeaderTitle;
	private Button buttonHeaderBank;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_detail_result);
		
		acPr1 = 0;
		acRe1 = 0;
		ipPr1 = 0;
		ipRe1 = 0;
		acPr2 = 0;
		acRe2 = 0;
		ipPr2 = 0;
		ipRe2 = 0;
		ac1Fmeasure = 0;
		ip1Fmeasure = 0;
		PLbeta1 = 0;
		ac2Fmeasure = 0;
		ip2Fmeasure = 0;
		PLbeta2 = 0;
		
		getResult();
		findView();
		showDbList();
		
		appListView.setOnItemClickListener(DBItemClickListener);
	}
	private void getResult() {
		Bundle resultBundle = DetailResultSub3Activity.this.getIntent().getExtras();
		apkFileName = resultBundle.getString("apkFileName");
		apkposition = resultBundle.getInt("apkposition");
		showVar = resultBundle.getInt("showVar");
		privacyRisk = resultBundle.getDouble("privacyRisk");
		apkBitmap = (Bitmap) DetailResultSub3Activity.this.getIntent().getParcelableExtra("apkBitmap");
		tableNameList = resultBundle.getStringArrayList("tableNameList");
		columnNameList = resultBundle.getStringArrayList("columnNameList");
		acPrivcyData = resultBundle.getStringArrayList("acPrivcyData");
		ac1PrivcyData = resultBundle.getStringArrayList("ac1PrivcyData");
		ac2PrivcyData = resultBundle.getStringArrayList("ac2PrivcyData");
		ipPrivcyData = resultBundle.getStringArrayList("ipPrivcyData");
		ip1PrivcyData = resultBundle.getStringArrayList("ip1PrivcyData");
		ip2PrivcyData = resultBundle.getStringArrayList("ip2PrivcyData");
		ipPrivcyData = resultBundle.getStringArrayList("ipPrivcyData");		
		databaseName = resultBundle.getStringArrayList("databaseName");
		columnNameList = resultBundle.getStringArrayList("columnNameList");
		PLbetaList = (ArrayList<Double>) DetailResultSub3Activity.this.getIntent().getSerializableExtra("PLbetaList");
		SortPLbetaList = (ArrayList<Double>) DetailResultSub3Activity.this.getIntent().getSerializableExtra("SortPLbetaList");
		filePlNonZero = (ArrayList<Integer>) DetailResultSub3Activity.this.getIntent().getSerializableExtra("filePlNonZero");
		
		analysisDbFilePath = pasteFilePath + File.separator + "copyDB"
				+ File.separator + ApkFileSub3Activity.mlistAppInfo.get(apkposition).getPkgName()
				+ File.separator;
		
		attackCriteria = ApkFileSub3Activity.attackCriteria;
		individualPerception = ApkFileSub3Activity.individualPerception;
	}
	
	private void findView(){
		
		
		textViewHeaderTitle = (TextView) findViewById(R.id.textViewHeaderTitle);
		buttonHeaderBank = (Button) findViewById(R.id.buttonHeaderBank);
		buttonHeaderBank.setVisibility(View.VISIBLE);
		textViewHeaderTitle
				.setText(R.string.textViewHeadTitleADAResultDetail_sub3);
		buttonHeaderBank.setOnClickListener(BankClickListener);
		
		
		appListView = (ListView) findViewById(R.id.listView1);
		textViewApkFileName = (TextView) findViewById(R.id.textViewApkFileName);
		imageViewApkIcon = (ImageView) findViewById(R.id.imageViewApkIcon);
		imageViewFolder = (ImageView) findViewById(R.id.imageViewFolder);
		showResult = (Button) findViewById(R.id.button1);
		showResult.setOnClickListener(showAlertDialog);
		
		textViewApkFileName.setText(apkFileName);
		imageViewApkIcon.setImageBitmap(apkBitmap);
		//appListView.setOnItemClickListener(PRAPDetailClickListener);
		
		
	}
	
	private Button.OnClickListener BankClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			// Intent intent = new Intent();
			// intent.setClass(ResultActivity.this, ApkFileSub3Activity.class);
			// startActivity(intent);
			finish();

		}
	};
	
	
	public void showDbList() {

		File dbFile = new File(pasteFilePath + File.separator + "copyDB"
				+ File.separator + ApkFileSub3Activity.mlistAppInfo.get(apkposition).getPkgName()
				+ File.separator);

		Filename = dbFile.list();
		Arrays.sort(Filename);
			appListView.setAdapter(new ArrayAdapter<String>(
					DetailResultSub3Activity.this, android.R.layout.simple_list_item_1,
					Filename));
	}
	
	private ListView.OnItemClickListener DBItemClickListener = new ListView.OnItemClickListener(){

		@Override
		public void onItemClick(AdapterView<?> arg0, View view,
				final int position, long id) {
			// TODO Auto-generated method stub
			if (showVar == 1) {
				showVar = 2;
			} else if (showVar == 2) {
				showVar = 3;
			}
			if (appListView.getItemAtPosition(0).toString() == "this app no such .db file...") {
				showVar = 1;
			}
			final CharSequence strDialogTitle = "分析內存資料庫中";
			final CharSequence strDialogBody = "分析"
					+ appListView.getItemAtPosition(position).toString()
					+ "中！";
			PDialog = ProgressDialog.show(DetailResultSub3Activity.this,
					strDialogTitle, strDialogBody, true);

			new Thread() {
				public void run() {
					try {
						sleep(1000);
						analysisDB(position);
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						PDialog.dismiss();
					}
				}
			}.start();

			Toast.makeText(DetailResultSub3Activity.this,
						"你選擇了 " + Filename[position],
						Toast.LENGTH_LONG).show();

			if (clickNumber == 0) {

				databaseName.set(0,
						"Ac_"
								+ appListView.getItemAtPosition(position)
										.toString());
				databaseName.set(1,
						"Ip_"
								+ appListView.getItemAtPosition(position)
										.toString());

			} else {
				databaseName.set(2,
						"Ac_"
								+ appListView.getItemAtPosition(position)
										.toString());
				databaseName.set(3,
						"Ip_"
								+ appListView.getItemAtPosition(position)
										.toString());
			}
		}
		
	};
	
	public void analysisDB(int position) {

		if (!Filename[position].endsWith("-journal")) {

			tableNameList.clear();
			columnNameList.clear();
			SQLiteDatabase db = SQLiteDatabase.openDatabase(analysisDbFilePath
					+ Filename[position], null, 0);

			acPrivcyData = new ArrayList<String>(Arrays.asList("", "", "", "",
					"", "", "", ""));
			ipPrivcyData = new ArrayList<String>(Arrays.asList("", "", "", "",
					"", "", "", ""));
			selectTableName(db);
			// selectColumnName(db);
			selectPrivacy(db);

		}
	}
	public void selectTableName(SQLiteDatabase db) {

		C = db.rawQuery("SELECT tbl_name FROM sqlite_master GROUP BY tbl_name",
				null);

		while (C.moveToNext()) {

			for (int x = 0; x < C.getColumnCount(); x++) {

				String tableName = C.getString(x);
				tableNameList.add(tableName);
			}
		}
		C.close();
		Log.d("table Name", tableNameList.toString());
	}

	@SuppressLint("NewApi")
	public void selectPrivacy(SQLiteDatabase db) {

		String columnValues = null;
		for (int x = 0; x < tableNameList.size(); x++) {
			columnNameList.clear();

			C = db.rawQuery("PRAGMA table_info(" + tableNameList.get(x) + ")",
					null);

			while (C.moveToNext()) {

				for (int i = 1; i < C.getColumnCount(); i = i + 6) {

					String columnName = C.getString(i);
					columnNameList.add(columnName);
				}
			}
			C.close();
			for (int y = 0; y < columnNameList.size(); y++) {

				// columnValues = 0;
				for (int ac = 0; ac < attackCriteria.length; ac++) {
					// columnValues = null;
					// System.out.println("column size "+columnNameList.size());
					// columnValuesList.clear();
					C = db.rawQuery("SELECT " + columnNameList.get(y)
							+ " FROM " + tableNameList.get(x) + " WHERE "
							+ columnNameList.get(y) + " Like '%"
							+ attackCriteria[ac] + "%' group by "
							+ columnNameList.get(y), null);

					while (C.moveToNext()) {

						if (C.getType(0) != 4) {
							columnValues = String.valueOf(C.getString(0));
						}
					}
					C.close();

					switch (ac) {
					case 0:
						if (columnValues != null) {

							acPrivcyData.set(0, attackCriteria[0]);
							columnValues = null;
						}
						break;
					case 1:
						if (columnValues != null) {

							acPrivcyData.set(1, attackCriteria[1]);
							columnValues = null;
						}
						break;
					case 2:
						if (columnValues != null) {

							acPrivcyData.set(2, attackCriteria[2]);
							columnValues = null;
						}
						break;
					case 3:
						if (columnValues != null) {

							acPrivcyData.set(3, attackCriteria[3]);
							columnValues = null;
						}
						break;
					case 4:
						if (columnValues != null) {

							acPrivcyData.set(4, attackCriteria[4]);
							columnValues = null;
						}
						break;
					case 5:
						if (columnValues != null) {

							acPrivcyData.set(5, attackCriteria[5]);
							columnValues = null;
						}
						break;
					case 6:
						if (columnValues != null) {

							acPrivcyData.set(6, attackCriteria[6]);
							columnValues = null;
						}
						break;
					case 7:
						if (columnValues != null) {

							acPrivcyData.set(7, "10/8");
							columnValues = null;
						}
						break;
					}

				}

				for (int ip = 0; ip < individualPerception.length; ip++) {
					// columnValues = null;
					// System.out.println("column size "+columnNameList.size());
					// columnValuesList.clear();
					C = db.rawQuery("SELECT " + columnNameList.get(y)
							+ " FROM " + tableNameList.get(x) + " WHERE "
							+ columnNameList.get(y) + " Like '%"
							+ individualPerception[ip] + "%' group by "
							+ columnNameList.get(y), null);

					while (C.moveToNext()) {

						if (C.getType(0) != 4) {
							columnValues = String.valueOf(C.getString(0));
						}
					}
					C.close();

					switch (ip) {
					case 0:
						if (columnValues != null) {

							ipPrivcyData.set(0, individualPerception[0]);
							columnValues = null;
						}
						break;
					case 1:
						if (columnValues != null) {

							ipPrivcyData.set(1, individualPerception[1]);
							columnValues = null;
						}
						break;
					case 2:
						if (columnValues != null) {

							ipPrivcyData.set(2, individualPerception[2]);
							columnValues = null;
						}
						break;
					case 3:
						if (columnValues != null) {

							ipPrivcyData.set(3, individualPerception[3]);
							columnValues = null;
						}
						break;
					case 4:
						if (columnValues != null) {

							ipPrivcyData.set(4, individualPerception[4]);
							columnValues = null;
						}
						break;
					case 5:
						if (columnValues != null) {

							ipPrivcyData.set(5, individualPerception[5]);
							columnValues = null;
						}
						break;
					case 6:
						if (columnValues != null) {

							ipPrivcyData.set(6, individualPerception[6]);
							columnValues = null;
						}
						break;
					}
				}

			}
		}
		System.out.println("clickNumber" + clickNumber);
		if (clickNumber == 0) {
			ac1PrivcyData = acPrivcyData;
			ip1PrivcyData = acPrivcyData;
		} else {
			ac2PrivcyData = acPrivcyData;
			ip2PrivcyData = ipPrivcyData;
		}
		Log.d("column Name", columnNameList.toString());
		Log.d("AC Privacy Value", acPrivcyData.toString());
		Log.d("IP Privacy Value", ipPrivcyData.toString());

		double PLbeta = (double) ((beta * calculationAC()) + ((1 - beta) * calculationIP()));
		if (clickNumber == 0) {
			PLbeta1 = PLbeta;
		} else {
			PLbeta2 = PLbeta;
		}

		System.out.println((int) (Math.round(PLbeta * 100.0)) / 100.0);
	}
	
	public double calculationAC() {

		double acRe = 0;
		double acPr = 0;
		double acFmeasure = 0;
		int wNameAc = 2, wPhoneAc = 3, wAddAc = 3, wEmailAc = 2, wPasswordAc = 4, wContentAc = 3, wIdAc = 2, wBdayAc = 1;
		int acReD = wNameAc + wPhoneAc + wAddAc + wEmailAc + wPasswordAc
				+ wContentAc + wIdAc + wBdayAc;

		if (acPrivcyData.get(0) == "") {

			wNameAc = 0;
		} else {

			wNameAc = 2;
		}
		if (acPrivcyData.get(1) == "") {

			wPhoneAc = 0;
		} else {

			wPhoneAc = 3;
		}
		if (acPrivcyData.get(2) == "") {

			wAddAc = 0;
		} else {

			wAddAc = 3;
		}
		if (acPrivcyData.get(3) == "") {

			wEmailAc = 0;
		} else {

			wEmailAc = 2;
		}
		if (acPrivcyData.get(4) == "") {

			wPasswordAc = 0;
		} else {

			wPasswordAc = 4;
		}
		if (acPrivcyData.get(5) == "") {

			wContentAc = 0;
		} else {

			wContentAc = 3;
		}
		if (acPrivcyData.get(6) == "") {

			wIdAc = 0;
		} else {

			wIdAc = 2;
		}
		if (acPrivcyData.get(7) == "") {

			wBdayAc = 0;
		} else {

			wBdayAc = 1;
		}

		if ((wNameAc + wPhoneAc + wAddAc + wEmailAc + wPasswordAc + wContentAc
				+ wIdAc + wBdayAc) != 0) {

			acPr = (double) (wNameAc + wPhoneAc + wAddAc + wEmailAc
					+ wPasswordAc + wContentAc + wIdAc + wBdayAc)
					/ (double) (wNameAc + wPhoneAc + wAddAc + wEmailAc
							+ wPasswordAc + wContentAc + wIdAc + wBdayAc);
			acPr = (int) (Math.round(acPr * 100.0)) / 100.0;
			acRe = (double) (wNameAc + wPhoneAc + wAddAc + wEmailAc
					+ wPasswordAc + wContentAc + wIdAc + wBdayAc)
					/ (double) acReD;
			acRe = (int) (Math.round(acRe * 100.0)) / 100.0;
			acFmeasure = (double) acPr * acRe
					/ (double) ((alpha * acRe) + ((1 - alpha) * acPr));
			acFmeasure = (int) (Math.round(acFmeasure * 100.0)) / 100.0;

		} else {

			acPr = 0;
			acRe = 0;
			acFmeasure = 0;
		}

		if (clickNumber == 0) {

			acPr1 = acPr;
			acRe1 = acRe;
			ac1Fmeasure = acFmeasure;
		} else {

			acPr2 = acPr;
			acRe2 = acRe;
			ac2Fmeasure = acFmeasure;
		}

		System.out.println("acPr: " + acPr + " acRe: " + acRe + " acFmeasure: "
				+ acFmeasure);
		return acFmeasure;
		// System.out.print(capturPrivcyData);

	}

	public double calculationIP() {

		double ipRe = 0;
		double ipPr = 0;
		double ipFmeasure = 0;
		Bundle cmBundle = this.getIntent().getExtras();
		int wNameIp = cmBundle.getInt("nameW"), wPhoneIp = cmBundle
				.getInt("phoneW"), wAddIp = cmBundle.getInt("cityW"), wEmailIp = cmBundle
				.getInt("mailW"), wPasswordIp = cmBundle
				.getInt("passswordW"), wContentIp = cmBundle
				.getInt("contentW"), wIdIp = cmBundle.getInt("idW"), wBdayIp = cmBundle
				.getInt("bdayW");
		//int wNameIp = 2, wPhoneIp = 3, wAddIp = 3, wEmailIp = 2, wPasswordIp = 4, wContentIp = 3, wIdIp = 2;

		int ipReD = wNameIp + wPhoneIp + wAddIp + wEmailIp + wPasswordIp
				+ wContentIp + wIdIp;
		if (ipPrivcyData.get(0) == "") {

			wNameIp = 0;
		} else {

			wNameIp = 2;
		}
		if (ipPrivcyData.get(1) == "") {

			wPhoneIp = 0;
		} else {

			wPhoneIp = 3;
		}
		if (ipPrivcyData.get(2) == "") {

			wAddIp = 0;
		} else {

			wAddIp = 3;
		}
		if (ipPrivcyData.get(3) == "") {

			wEmailIp = 0;
		} else {

			wEmailIp = 2;
		}
		if (ipPrivcyData.get(4) == "") {

			wPasswordIp = 0;
		} else {

			wPasswordIp = 4;
		}
		if (ipPrivcyData.get(5) == "") {

			wContentIp = 0;
		} else {

			wContentIp = 3;
		}
		if (ipPrivcyData.get(6) == "") {

			wIdIp = 0;
		} else {

			wIdIp = 2;
		}
		if (ipPrivcyData.get(7) == "") {

			wBdayIp = 0;
		} else {

			wBdayIp = 2;
		}

		if ((wNameIp + wPhoneIp + wAddIp + wEmailIp + wPasswordIp + wContentIp + wIdIp) != 0) {

			ipPr = (double) (wNameIp + wPhoneIp + wAddIp + wEmailIp
					+ wPasswordIp + wContentIp + wIdIp + wBdayIp)
					/ (double) (wNameIp + wPhoneIp + wAddIp + wEmailIp
							+ wPasswordIp + wContentIp + wIdIp + wBdayIp);
			ipPr = (int) (Math.round(ipPr * 100.0)) / 100.0;

			ipRe = (double) (wNameIp + wPhoneIp + wAddIp + wEmailIp
					+ wPasswordIp + wContentIp + wIdIp + wBdayIp)
					/ (double) ipReD;
			ipRe = (int) (Math.round(ipRe * 100.0)) / 100.0;

			ipFmeasure = (double) ipPr * ipRe
					/ (double) ((alpha * ipRe) + ((1 - alpha) * ipPr));
			ipFmeasure = (int) (Math.round(ipFmeasure * 100.0)) / 100.0;
		} else {

			ipPr = 0;
			ipRe = 0;
			ipFmeasure = 0;
		}

		if (clickNumber == 0) {

			ipPr1 = ipPr;
			ipRe1 = ipRe;
			ip1Fmeasure = ipFmeasure;
			clickNumber++;
		} else {

			ipPr2 = ipPr;
			ipRe2 = ipRe;
			ip2Fmeasure = ipFmeasure;
			clickNumber--;
		}
		System.out.println("ipPr: " + ipPr + " ipRe: " + ipRe + " ipFmeasure: "
				+ ipFmeasure);
		return ipFmeasure;
		// System.out.print(capturPrivcyData);

	}
	
	OnClickListener showAlertDialog = new OnClickListener() {

		@Override
		public void onClick(View v) {

			if (showVar != 0) {

				AlertDialog.Builder builderSingle = new AlertDialog.Builder(
						DetailResultSub3Activity.this);
//				builderSingle.setIcon(R.drawable.icon);
				builderSingle.setTitle(R.string.sub3_verifyResult_Privacy_Risk_Evaluation);
				final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
						DetailResultSub3Activity.this,
						android.R.layout.select_dialog_item);

				if (showVar == 1) {
					arrayAdapter.add(getResources().getString(R.string.sub3_verifyResult_Dialog_result));

				} else if (showVar == 2) {
					arrayAdapter.add(databaseName.get(0));
					arrayAdapter.add(databaseName.get(1));
					arrayAdapter.add(getResources().getString(R.string.sub3_verifyResult_Dialog_result));
				} else if (showVar == 3) {
					arrayAdapter.add(databaseName.get(0));
					arrayAdapter.add(databaseName.get(1));
					arrayAdapter.add(databaseName.get(2));
					arrayAdapter.add(databaseName.get(3));
					arrayAdapter.add(getResources().getString(R.string.sub3_verifyResult_Dialog_result));
				}

				builderSingle.setNegativeButton(getResources().getString(R.string.sub3_verifyResult_Dialog_ok),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						});

				builderSingle.setAdapter(arrayAdapter,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								String tit;
								int len;
								int len2;
								int positionVar = 0;
								String FvalueString;
								if (showVar == 1) {
									positionVar = 4;
								}
								if (showVar == 2) {
									if (which == 2) {
										positionVar = 4;
									} else {
										positionVar = which;
									}
								}
								if (showVar == 3) {
									positionVar = which;
								}

								switch (positionVar) {
								case 0:
									tit = databaseName.get(0);
									titleString = new SpannableString(tit);
									len = tit.length();
									titleString.setSpan(new RelativeSizeSpan(
											0.7f), 2, len,
											Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									AnalysisResult = new SpannableString("");
									if (ac1PrivcyData.get(0) != "") {
										AnalysisResult = new SpannableString(
												"(姓名, "
														+ ac1PrivcyData.get(0)
														+ ")\n");
									}
									if (ac1PrivcyData.get(1) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(手機, "
														+ ac1PrivcyData.get(1)
														+ ")\n");
									}
									if (ac1PrivcyData.get(2) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(居住城市, "
														+ ac1PrivcyData.get(2)
														+ ")\n");
									}
									if (ac1PrivcyData.get(3) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "( 電子郵件, "
														+ ac1PrivcyData.get(3)
														+ ")\n");
									}
									if (ac1PrivcyData.get(4) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(密碼, "
														+ ac1PrivcyData.get(4)
														+ ")\n");
									}
									if (ac1PrivcyData.get(5) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(內容, "
														+ ac1PrivcyData.get(5)
														+ ")\n");
									}
									if (ac1PrivcyData.get(6) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(ID, "
														+ ac1PrivcyData.get(6)
														+ ")\n");
									}
									if (ac1PrivcyData.get(7) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(生日, "
														+ ac1PrivcyData.get(7)
														+ ")\n");
									}
									AnalysisResult = new SpannableString(
											AnalysisResult
													+ "\n\n正確性 = "
													+ acPr1 + "\n"
													+ "完整性 = " + acRe1
													+ "\n"
													+ "資訊遺失 = ");

									len = AnalysisResult.length();
									FvalueString = ac1Fmeasure + " ";
									AnalysisResult = new SpannableString(
											AnalysisResult + FvalueString);
									len2 = AnalysisResult.length();
									if (ac1Fmeasure > 0.5) {
										AnalysisResult
												.setSpan(
														new ForegroundColorSpan(
																Color.RED),
														len,
														len2,
														Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									}
									len = AnalysisResult.length();
									AnalysisResult.setSpan(
											new RelativeSizeSpan(1.5f), 0, len,
											Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

									break;

								case 1:
									tit = databaseName.get(1);
									titleString = new SpannableString(tit);
									len = tit.length();
									titleString.setSpan(new RelativeSizeSpan(
											0.7f), 2, len,
											Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									;
									AnalysisResult = new SpannableString("");
									if (ip1PrivcyData.get(0) != "") {
										AnalysisResult = new SpannableString(
												"(姓名, "
														+ ip1PrivcyData.get(0)
														+ ")\n");
									}
									if (ip1PrivcyData.get(1) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(手機, "
														+ ip1PrivcyData.get(1)
														+ ")\n");
									}
									if (ip1PrivcyData.get(2) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(居住城市, "
														+ ip1PrivcyData.get(2)
														+ ")\n");
									}
									if (ip1PrivcyData.get(3) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(電子郵件, "
														+ ip1PrivcyData.get(3)
														+ ")\n");
									}
									if (ip1PrivcyData.get(4) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(密碼, "
														+ ip1PrivcyData.get(4)
														+ ")\n");
									}
									if (ip1PrivcyData.get(5) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(內容, "
														+ ip1PrivcyData.get(5)
														+ ")\n");
									}
									if (ip1PrivcyData.get(6) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(ID, "
														+ ip1PrivcyData.get(6)
														+ ")\n");
									}

									AnalysisResult = new SpannableString(
											AnalysisResult
													+ "\n\n正確性 = "
													+ ipPr1 + "\n"
													+ "完整性 = " + ipRe1
													+ "\n"
													+ "資訊遺失 = ");
									len = AnalysisResult.length();
									FvalueString = ip1Fmeasure + " ";
									AnalysisResult = new SpannableString(
											AnalysisResult + FvalueString);
									len2 = AnalysisResult.length();
									if (ip1Fmeasure > 0.5) {
										AnalysisResult
												.setSpan(
														new ForegroundColorSpan(
																Color.RED),
														len,
														len2,
														Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									}
									len = AnalysisResult.length();
									AnalysisResult.setSpan(
											new RelativeSizeSpan(1.5f), 0, len,
											Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

									break;
								case 2:
									tit = databaseName.get(2);
									titleString = new SpannableString(tit);
									len = tit.length();
									titleString.setSpan(new RelativeSizeSpan(
											0.7f), 2, len,
											Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									AnalysisResult = new SpannableString("");
									if (ac2PrivcyData.get(0) != "") {
										AnalysisResult = new SpannableString(
												"(姓名, "
														+ ac2PrivcyData.get(0)
														+ ")\n");
									}
									if (ac2PrivcyData.get(1) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(手機, "
														+ ac2PrivcyData.get(1)
														+ ")\n");
									}
									if (ac2PrivcyData.get(2) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(居住城市, "
														+ ac2PrivcyData.get(2)
														+ ")\n");
									}
									if (ac2PrivcyData.get(3) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(電子郵件, "
														+ ac2PrivcyData.get(3)
														+ ")\n");
									}
									if (ac2PrivcyData.get(4) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(密碼, "
														+ ac2PrivcyData.get(4)
														+ ")\n");
									}
									if (ac2PrivcyData.get(5) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(內容, "
														+ ac2PrivcyData.get(5)
														+ ")\n");
									}
									if (ac2PrivcyData.get(6) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(ID, "
														+ ac2PrivcyData.get(6)
														+ ")\n");
									}
									if (ac2PrivcyData.get(7) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(生日, "
														+ ac2PrivcyData.get(7)
														+ ")\n");
									}
									AnalysisResult = new SpannableString(
											AnalysisResult
													+ "\n\n正確性 = "
													+ acPr2 + "\n"
													+ "完整性 = " + acRe2
													+ "\n"
													+ "資訊遺失= ");
									len = AnalysisResult.length();
									FvalueString = ac2Fmeasure + " ";
									AnalysisResult = new SpannableString(
											AnalysisResult + FvalueString);
									len2 = AnalysisResult.length();
									if (ac2Fmeasure > 0.5) {
										AnalysisResult
												.setSpan(
														new ForegroundColorSpan(
																Color.RED),
														len,
														len2,
														Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									}
									len = AnalysisResult.length();
									AnalysisResult.setSpan(
											new RelativeSizeSpan(1.5f), 0, len,
											Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									break;

								case 3:
									tit = databaseName.get(3);
									titleString = new SpannableString(tit);
									len = tit.length();
									titleString.setSpan(new RelativeSizeSpan(
											0.7f), 2, len,
											Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									AnalysisResult = new SpannableString("");
									if (ip2PrivcyData.get(0) != "") {
										AnalysisResult = new SpannableString(
												"(姓名, "
														+ ip2PrivcyData.get(0)
														+ ")\n");
									}
									if (ip2PrivcyData.get(1) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(手機, "
														+ ip2PrivcyData.get(1)
														+ ")\n");
									}
									if (ip2PrivcyData.get(2) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(居住城市, "
														+ ip2PrivcyData.get(2)
														+ ")\n");
									}
									if (ip2PrivcyData.get(3) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(電子郵件, "
														+ ip2PrivcyData.get(3)
														+ ")\n");
									}
									if (ip2PrivcyData.get(4) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(密碼, "
														+ ip2PrivcyData.get(4)
														+ ")\n");
									}
									if (ip2PrivcyData.get(5) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(內容, "
														+ ip2PrivcyData.get(5)
														+ ")\n");
									}
									if (ip2PrivcyData.get(6) != "") {
										AnalysisResult = new SpannableString(
												AnalysisResult + "(ID, "
														+ ip2PrivcyData.get(6)
														+ ")\n");
									}

									AnalysisResult = new SpannableString(
											AnalysisResult
													+ "\n\n正確性= "
													+ ipPr2 + "\n"
													+ "完整性 = " + ipRe2
													+ "\n"
													+ "資訊遺失 = ");
									len = AnalysisResult.length();
									FvalueString = ip2Fmeasure + " ";
									AnalysisResult = new SpannableString(
											AnalysisResult + FvalueString);
									len2 = AnalysisResult.length();
									if (ip2Fmeasure > 0.5) {
										AnalysisResult
												.setSpan(
														new ForegroundColorSpan(
																Color.RED),
														len,
														len2,
														Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									}
									len = AnalysisResult.length();
									AnalysisResult.setSpan(
											new RelativeSizeSpan(1.5f), 0, len,
											Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									break;

								case 4:
									if (appListView.getItemAtPosition(0)
											.toString() != "this app no such .db file...") {

										AnalysisResult = new SpannableString("");
										for (int i = 0; i < filePlNonZero
												.size(); i++) {
											AnalysisResult = new SpannableString(
													AnalysisResult
															+ Filename[filePlNonZero
																	.get(i)]
															+ " : \n"+DetailResultSub3Activity.this.getResources().getString(
																	R.string.sub3_verifyResult_Privacy_Leakage)+"= "
															+ PLbetaList
																	.get(filePlNonZero
																			.get(i))
															+ "\n\n");
										}

										AnalysisResult = new SpannableString(
												AnalysisResult
														+ DetailResultSub3Activity.this.getResources().getString(
																R.string.sub3_verifyResult_Privacy_Risk)+" = ");
										len = AnalysisResult.length();
										AnalysisResult = new SpannableString(
												AnalysisResult + ""
														+ privacyRisk
														+ "\n\n\n");
										len2 = AnalysisResult.length();
										if (privacyRisk > 0.5) {
											AnalysisResult.setSpan(
													new ForegroundColorSpan(
															Color.rgb(255, 0, 0)),
													len,
													len2,
													Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
											SpannableString toastStSpannableString;
											toastStSpannableString = new SpannableString(DetailResultSub3Activity.this.getResources().getString(
													R.string.sub3_verifyResult_Risk_high));
											len = toastStSpannableString
													.length();
											toastStSpannableString
													.setSpan(
															new RelativeSizeSpan(
																	2f),
															0,
															len,
															Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
											toastStSpannableString.setSpan(
													new ForegroundColorSpan(
															Color.rgb(255, 0, 0)),
													0,
													len,
													Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
											Toast.makeText(
													DetailResultSub3Activity.this,
													toastStSpannableString,
													Toast.LENGTH_LONG).show();
										} else if (0.5 > privacyRisk
												&& privacyRisk > 0.25) {
											AnalysisResult
													.setSpan(
															new ForegroundColorSpan(
																	Color.rgb(
																			255,
																			110,
																			0)),
															len,
															len2,
															Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
											SpannableString toastStSpannableString;
											toastStSpannableString = new SpannableString(DetailResultSub3Activity.this.getResources().getString(
													R.string.sub3_verifyResult_Risk_mid));
											len = toastStSpannableString
													.length();
											toastStSpannableString
													.setSpan(
															new RelativeSizeSpan(
																	2f),
															0,
															len,
															Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
											toastStSpannableString
													.setSpan(
															new ForegroundColorSpan(
																	Color.rgb(
																			255,
																			110,
																			0)),
															0,
															len,
															Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
											Toast.makeText(
													DetailResultSub3Activity.this,
													toastStSpannableString,
													Toast.LENGTH_LONG).show();
										} else if (0.25 > privacyRisk) {
											AnalysisResult
													.setSpan(
															new ForegroundColorSpan(
																	Color.rgb(
																			0,
																			128,
																			64)),
															len,
															len2,
															Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
											SpannableString toastStSpannableString;
											toastStSpannableString = new SpannableString(DetailResultSub3Activity.this.getResources().getString(
													R.string.sub3_verifyResult_Risk_low));
											len = toastStSpannableString
													.length();
											toastStSpannableString
													.setSpan(
															new RelativeSizeSpan(
																	2f),
															0,
															len,
															Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
											toastStSpannableString
													.setSpan(
															new ForegroundColorSpan(
																	Color.rgb(
																			0,
																			128,
																			64)),
															0,
															len,
															Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
											Toast.makeText(
													DetailResultSub3Activity.this,
													toastStSpannableString,
													Toast.LENGTH_LONG).show();
										}
										len = AnalysisResult.length();
										AnalysisResult
												.setSpan(
														new RelativeSizeSpan(
																1.5f),
														0,
														len,
														Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									} else {
										AnalysisResult = new SpannableString(
												DetailResultSub3Activity.this.getResources().getString(
														R.string.sub3_verifyResult_Privacy_Risk)+" = ");
										len = AnalysisResult.length();
										AnalysisResult = new SpannableString(
												AnalysisResult + "0.0"
														+ "\n\n\n");
										len2 = AnalysisResult.length();
										AnalysisResult.setSpan(
												new ForegroundColorSpan(Color
														.rgb(0, 128, 64)),
												len,
												len2,
												Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
										SpannableString toastStSpannableString;
										toastStSpannableString = new SpannableString(
												DetailResultSub3Activity.this.getResources().getString(
														R.string.sub3_verifyResult_Risk_low));
										len = toastStSpannableString.length();
										toastStSpannableString
												.setSpan(
														new RelativeSizeSpan(2f),
														0,
														len,
														Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
										toastStSpannableString.setSpan(
												new ForegroundColorSpan(Color
														.rgb(0, 128, 64)),
												0,
												len,
												Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
										Toast.makeText(DetailResultSub3Activity.this,
												toastStSpannableString,
												Toast.LENGTH_LONG).show();
										len = AnalysisResult.length();
										AnalysisResult
												.setSpan(
														new RelativeSizeSpan(
																1.5f),
														0,
														len,
														Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
									}

									break;
								}

								AlertDialog.Builder builderInner = new AlertDialog.Builder(
										DetailResultSub3Activity.this);

								builderInner.setMessage(AnalysisResult);

								if (which == 4) {
									builderInner.setTitle("Over All Privacy Risk");
								}else {
									builderInner.setTitle(titleString);
								}
								
								builderInner.setPositiveButton("確認",
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												showAlertDialog.onClick(null);
												AnalysisResult = new SpannableString(
														"");
											}
										});
								builderInner.show();
							}
						});
				builderSingle.show();
			}
		}

	};
}
