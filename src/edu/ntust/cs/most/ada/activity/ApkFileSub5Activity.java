package edu.ntust.cs.most.ada.activity;

import java.util.ArrayList;
import java.util.List;

import edu.ntust.cs.most.ada.domain.APKManifest;
import edu.ntust.cs.most.ada.task.AMDRVVerifyAsyncTask;
import edu.ntust.cs.most.ada.util.APKFileHandler;
import edu.ntust.cs.most.ada.util.ImageTransformer;
import edu.ntust.cs.most.ada.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author Ssu-Wei,Tang
 *
 */
public class ApkFileSub5Activity extends Activity {

	private TextView textViewApkFileName;
	private ImageView imageViewApkIcon;
	private static final int REQUEST_PATH = 1;
	public static String apkFileName;
	public static String apkFilePath;
	public static int versionCode;
	public int s = 0;
	private APKManifest apkManifest;

	public static ProgressDialog progressDialog;
	private WifiManager wifiManager;
	private DisplayMetrics metrics;
	public static Context mContext;

	private ImageView imageViewFolder;
	public static Button buttonVerify;
	public static ImageButton buttonSearch;
	// public EditText editTextApkFile;
	private AutoCompleteTextView autoCompleteTextViewAppName;
	private String[] applicationName;
	private TextView textViewHeaderTitle;
	private Button buttonHeaderBank;
	 private int index=0;
	 private AlertDialog alert;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_apkfile_sub5);
		mContext = this;
		findView();
		setListeners();

	}

	@SuppressLint("NewApi")
	private void setListeners() {

		buttonVerify.setOnClickListener(VerifyClickListener);
		imageViewApkIcon.setOnClickListener(BrowserClickListener);
		buttonSearch.setOnClickListener(SearchClickListener);
		buttonHeaderBank.setOnClickListener(BankClickListener);

		// imageViewApkIcon.setBackground(null);
	}

	private void findView() {

		getDisplay();
		buttonSearch = (ImageButton) findViewById(R.id.buttonSearch);
		autoCompleteTextViewAppName = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewApkApplicaltionName);

		buttonVerify = (Button) findViewById(R.id.buttonVerify);
		textViewApkFileName = (TextView) findViewById(R.id.textViewApkFileName);
		imageViewApkIcon = (ImageView) findViewById(R.id.imageViewApkIcon);
		imageViewFolder = (ImageView) findViewById(R.id.imageViewFolder);
		
	    textViewHeaderTitle = (TextView) findViewById(R.id.textViewHeaderTitle);
			buttonHeaderBank = (Button) findViewById(R.id.buttonHeaderBank);
			buttonHeaderBank.setVisibility(View.VISIBLE);
			textViewHeaderTitle.setText(R.string.buttonFunction_sub5);

		List<PackageInfo> appList = getAllApps(ApkFileSub5Activity.this);
		PackageInfo packageinfo;
		PackageManager packageManager = ApkFileSub5Activity.this
				.getPackageManager();

		applicationName = new String[appList.size()];

		for (int i = 0; i < appList.size(); i++) {
			packageinfo = appList.get(i);

			applicationName[i] = packageManager.getApplicationLabel(
					packageinfo.applicationInfo).toString();

		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, applicationName);
		autoCompleteTextViewAppName.setAdapter(adapter);

		setView();

	}

	private void getDisplay() {
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
	}

	public static List<PackageInfo> getAllApps(Context coNtext) {
		List<PackageInfo> apps = new ArrayList<PackageInfo>();
		PackageManager packageManager = coNtext.getPackageManager();
		List<PackageInfo> paklist = packageManager.getInstalledPackages(0);
		for (int i = 0; i < paklist.size(); i++) {
			PackageInfo pak = (PackageInfo) paklist.get(i);
			if ((pak.applicationInfo.flags & pak.applicationInfo.FLAG_SYSTEM) <= 0) {
				apps.add(pak);
			}
		}
		return apps;
	}

	@Override
	protected void onStop() {
		super.onStop();

		setView();

	}

	private Button.OnClickListener SearchClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (!autoCompleteTextViewAppName.getText().toString().equals("")) {
				PackageManager packageManager = ApkFileSub5Activity.this
						.getPackageManager();
				List<PackageInfo> appList = getAllApps(ApkFileSub5Activity.this);
				PackageInfo packageinfo;
				boolean found = false;
				for (int i = 0; i < appList.size(); i++) {
					packageinfo = appList.get(i);

					if (packageManager
							.getApplicationLabel(packageinfo.applicationInfo)
							.toString()
							.indexOf(
									autoCompleteTextViewAppName.getText()
											.toString()) == -1) {

					} else {
						found = true;
						textViewApkFileName
								.setText(packageinfo.applicationInfo.packageName
										+ ".apk");
						Bitmap apkBitmap = ImageTransformer
								.drawableToBitmap(packageManager
										.getApplicationIcon(packageinfo.applicationInfo));
						imageViewApkIcon.setImageBitmap(apkBitmap);

						try {
							ApplicationInfo AI = packageManager
									.getApplicationInfo(
											packageinfo.packageName,
											PackageManager.GET_META_DATA
													| PackageManager.GET_SHARED_LIBRARY_FILES);
							apkFilePath = AI.sourceDir;
							apkFileName = packageinfo.applicationInfo.packageName
									+ ".apk";

						} catch (NameNotFoundException e) {
							e.printStackTrace();
						}
						versionCode = APKFileHandler.getAPKVersionCode(
								mContext, apkFilePath);
						break;
					}

				}
				if (found == false) {
					getOkDialog(ApkFileSub5Activity.this, R.string.dialogWarm,
							R.string.dialogNoInstall, R.string.dialogOK);
				}
			} else {
				getOkDialog(ApkFileSub5Activity.this, R.string.dialogWarm,
						R.string.dialogEnterAppName, R.string.dialogOK);
			}

		}
	};
	private Button.OnClickListener BrowserClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			int in=showDialogButtonClick();
			
			System.out.println("in "+in);

//			ArrayList<String> extensions = new ArrayList<String>();
//			extensions.add(".apk");
//			Intent intent1 = new Intent(
//					ApkFileSub5Activity.this.getApplicationContext(),
//					ApkFileChooserActivity.class);
//			intent1.putStringArrayListExtra((String) ApkFileSub5Activity.this
//					.getResources().getText(R.string.filter), extensions);
//			startActivityForResult(intent1, REQUEST_PATH);
		}
	};

	private Button.OnClickListener VerifyClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {

			case R.id.buttonVerify:
				if (check()) {
					new AMDRVVerifyAsyncTask(ApkFileSub5Activity.this)
							.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					
				}
				break;
			}
		}
	};

	private boolean checkNetWorkConnect() {
		boolean checkCon = true;
		ConnectivityManager mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
		if (mNetworkInfo == null || !mNetworkInfo.isConnected()) {
			checkCon = false;
			wifiManager = (WifiManager) ApkFileSub5Activity.this
					.getSystemService(ApkFileActivity.WIFI_SERVICE);
			if (!wifiManager.isWifiEnabled()) {
				getOkAndCancelDialog(ApkFileSub5Activity.this,
						R.string.dialogWarm, R.string.dialogWifiOpen,
						R.string.dialogOpen, R.string.dialogCancel);
			} else {
				getOkDialog(ApkFileSub5Activity.this, R.string.dialogWarm,
						R.string.dialogChecknetwork, R.string.dialogOK);
			}
		}
		return checkCon;
	}

	protected boolean check() {
		boolean check = false;
		if (textViewApkFileName.getText().toString().equals(""))
			getOkDialog(ApkFileSub5Activity.this, R.string.dialogWarm,
					R.string.selectApkFile, R.string.dialogOK);
		else if (!textViewApkFileName.getText().toString().endsWith(".apk"))
			getOkDialog(ApkFileSub5Activity.this, R.string.dialogWarm,
					R.string.selectApkFile, R.string.dialogOK);
		else if (textViewApkFileName.getText().toString()
				.equals("apkFileName.apk"))
			getOkDialog(ApkFileSub5Activity.this, R.string.dialogWarm,
					R.string.selectApkFile, R.string.dialogOK);
		else
			check = checkNetWorkConnect();
		return check;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == REQUEST_PATH) {
			if (resultCode == RESULT_OK) {
				apkFileName = data.getStringExtra("fileName");
				apkFilePath = data.getStringExtra("filePath");
				versionCode = data.getIntExtra("versionCode", -1);
				textViewApkFileName.setText(apkFileName);
				Bitmap apkBitmap = ImageTransformer
						.drawableToBitmap(APKFileHandler.getApkIcon(
								ApkFileSub5Activity.this, apkFilePath));
				imageViewApkIcon.setImageBitmap(apkBitmap);

			}
		}
	}

	public static ProgressDialog showProgressDialog() {
		progressDialog = new ProgressDialog(mContext);
		progressDialog.setMessage(mContext.getResources().getString(
				R.string.conServer));
		progressDialog.setCancelable(false);
		progressDialog.show();
		return progressDialog;
	}

	public void getOkDialog(Context context, int title, int message,
			int dialogok) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setPositiveButton(dialogok,
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(android.content.DialogInterface dialog,
							int whichButton) {
						setResult(RESULT_OK);
					}
				});
		builder.create();
		builder.show();
	}

	private void getOkAndCancelDialog(Context context, int title, int message,
			int dialogok, int dialogcnacel) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setPositiveButton(dialogok,
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(android.content.DialogInterface dialog,
							int i) {
						setResult(RESULT_OK);
						wifiManager.setWifiEnabled(true);
					}
				});
		builder.setNegativeButton(dialogcnacel, null);
		builder.create();
		builder.show();
	}

	private void setView() {
		imageViewApkIcon.setImageBitmap(BitmapFactory.decodeResource(
				getResources(), R.drawable.apk));

		buttonSearch.setImageBitmap(BitmapFactory.decodeResource(
				getResources(), R.drawable.search));

		textViewApkFileName.setText(R.string.textViewapkFileDefault);

	}
	
	private Button.OnClickListener BankClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			// Intent intent = new Intent();
			// intent.setClass(ResultActivity.this, ApkFileActivity.class);
			// startActivity(intent);
			finish();

		}
	};
	
	
	 private int showDialogButtonClick() {
			
	        AlertDialog.Builder builder = 
	            new AlertDialog.Builder(mContext);
	        builder.setTitle("請選擇欲檢驗APK類型");
	         
	        final CharSequence[] choiceList =  {"已安裝", "未安裝"  };
	         
	         final int selected = -1; // does not select anything
	       
	         builder.setCancelable(true);
	        builder.setSingleChoiceItems(
	                choiceList, 
	                selected, 
	                new DialogInterface.OnClickListener() {
	             
	            @Override
	            public void onClick(
	                    DialogInterface dialog, 
	                    int which) {
	            
	            	
	            	if(which==1)
	            	{
	            		
	            		ArrayList<String> extensions = new ArrayList<String>();
	        			extensions.add(".apk");
	        			Intent intent1 = new Intent(
	        					ApkFileSub5Activity.this.getApplicationContext(),
	        					ApkFileChooserActivity.class);
	        			intent1.putStringArrayListExtra((String) ApkFileSub5Activity.this
	        					.getResources().getText(R.string.filter), extensions);
	        			startActivityForResult(intent1, REQUEST_PATH);
	            		
	            	}
	            	else
	            	{
	            		Intent intent1 = new Intent(
	        					ApkFileSub5Activity.this.getApplicationContext(),
	        					ApkFileChooserInstallActivity.class);
	        			
	        			startActivityForResult(intent1, REQUEST_PATH);
	            	}
	            	
	                
	                
	                alert.dismiss();
	              
	               
	            }
	           
	        });
	        alert = builder.create();
	        alert.show();
	        return  index;
	    }

}
